//
//  AppDelegate.swift
//  Finix Ramadan Calendar
//
//  Created by Md.Ballal Hossen on 18/4/18.
//  Copyright © 2018 Sujan. All rights reserved.
//

import UIKit
import UserNotifications



@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate {
  
   
    var window: UIWindow?
    let center = UNUserNotificationCenter.current()
    var backgroundSessionCompletionHandler: (() -> Void)?
    
    var ramadanData : NSArray!
    
    let downloadService = DownloadService()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
      
       
        center.delegate=self;
        let options: UNAuthorizationOptions = [.alert, .sound];
        // Swift
        center.requestAuthorization(options: options) {
            (granted, error) in
            if !granted {
                print("Something went wrong")
            }
        }
        
        // set FirstScreenViewController as initialviewcontroller
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let initialViewController: UIViewController? = storyboard.instantiateViewController(withIdentifier: "FirstScreenViewController")
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = initialViewController
        window?.makeKeyAndVisible()
        
       // Only for first run

        if UserDefaults.standard.object(forKey: "selectedDivision") != nil{
            
            // exist
        }
        else {
            // not exist
  
                print("First run")
                UserDefaults.standard.set(0, forKey: "selectedIndex")
                
                UserDefaults.standard.set("Dhaka", forKey: "selectedDivision")
            
                UserDefaults.standard.set("Dhaka", forKey: "selectedDistrict")
            
            
            //             UserDefaults.standard.set(1, forKey: "isPrayerAlarmOn")
            //             UserDefaults.standard.set(1, forKey: "isIfterAlarmOn")
            //             UserDefaults.standard.set(1, forKey: "isSehriAlarmOn")

        }
   return true
    }
    
    
    func application(_ application: UIApplication, handleEventsForBackgroundURLSession identifier: String, completionHandler: @escaping () -> Void) {
        
        backgroundSessionCompletionHandler = completionHandler
        
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        
        print("applicationWillResignActive")
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        print("applicationDidEnterBackground")
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        
        print("applicationWillEnterForeground")
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        print(UIApplication.shared.keyWindow?.rootViewController!)
        
        if UIApplication.shared.keyWindow?.rootViewController is FirstScreenViewController
        {
            let firstScreenVC =  UIApplication.shared.keyWindow?.rootViewController as! FirstScreenViewController
            
            
            
            if let rootVC =  firstScreenVC.presentedViewController
            {
                let navController = rootVC as! UINavigationController
                
                for viewController in navController.viewControllers {
                    
                    print(viewController)
                    
                    if viewController is MainViewController
                    {
                        viewController.viewDidLoad()
                        print("You have access to viewController loaded do whatever you want")
                        break
                        
                    }
                }
            }
            
            
        }
        
       
        
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        print("applicationWillTerminate")
    }
    
  
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        // Play sound and show alert to the user
        print(notification)
        completionHandler([.alert,.sound])
        
        
    }

    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        
        // Determine the user action
        switch response.actionIdentifier {
        case UNNotificationDismissActionIdentifier:
            print("Dismiss Action")
        case UNNotificationDefaultActionIdentifier:
            print("Default")
        case "Snooze":
            print("Snooze")
        case "Delete":
            print("Delete")
        default:
            print("Unknown action")
        }
        completionHandler()
    }

}

