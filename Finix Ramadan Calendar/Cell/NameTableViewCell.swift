//
//  NameTableViewCell.swift
//  Finix Ramadan Calendar
//
//  Created by Md.Ballal Hossen on 23/4/18.
//  Copyright © 2018 Sujan. All rights reserved.
//

import UIKit

class NameTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var arabicLabel: UILabel!
    @IBOutlet weak var englishLabel: UILabel!
    @IBOutlet weak var meaningLabel: UILabel!
    @IBOutlet weak var explanationLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
