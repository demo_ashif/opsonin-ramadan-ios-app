//
//  SingleSuraTableViewCell.swift
//  Finix Ramadan Calendar
//
//  Created by Md.Ballal Hossen on 17/5/18.
//  Copyright © 2018 Sujan. All rights reserved.
//

import UIKit

class SingleSuraTableViewCell: UITableViewCell {

    
    @IBOutlet weak var verseNoLabel: UILabel!
    
    @IBOutlet weak var verseArabicLabel: UILabel!
    
    @IBOutlet weak var pronunciationLabel: UILabel!
    @IBOutlet weak var banglaLabel: UILabel!
    
    @IBOutlet weak var englishLabel: UILabel!
    
    @IBOutlet weak var tapsirLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
