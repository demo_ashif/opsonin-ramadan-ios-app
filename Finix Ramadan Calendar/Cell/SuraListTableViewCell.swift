//
//  SuraListTableViewCell.swift
//  Finix Ramadan Calendar
//
//  Created by Md.Ballal Hossen on 17/5/18.
//  Copyright © 2018 Sujan. All rights reserved.
//

import UIKit

class SuraListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var suraNameBanglaLabel: UILabel!
    
    @IBOutlet weak var suraTypeLabel: UILabel!
    
    @IBOutlet weak var ayathNoLabel: UILabel!
    
    @IBOutlet weak var suraNameArabicLabel: UILabel!
    
    
    @IBOutlet weak var suraNoLabel: UILabel!
    
    
    @IBOutlet weak var playButton: UIButton!
    
    @IBOutlet weak var downloadButton: UIButton!
    
    
    @IBOutlet weak var suraNameEnglishLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
//class CustomButton : UIButton {
//
//    var name : String = ""
//
//
//    convenience init(name: String) {
//        self.init()
//        self.name = name
//
//    }
//}

