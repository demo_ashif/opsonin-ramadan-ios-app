//
//  Date+Helpers.swift
//  FajrWake
//
//  Created by Ali Mir on 9/11/17.
//  Copyright © 2017 com.AliMir. All rights reserved.
//

import Foundation

extension DateFormatter {
    
    static var splitTimeFormatter: (time: DateFormatter,hour: DateFormatter,min: DateFormatter, ampm: DateFormatter) = {
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "h:mm,a"
        let hourFormatter = DateFormatter()
        hourFormatter.dateFormat = "h"
        let minuteFormatter = DateFormatter()
        minuteFormatter.dateFormat = "mm"
        let ampmFormatter = DateFormatter()
        ampmFormatter.dateFormat = "a"
        return (time: timeFormatter,hour:hourFormatter,min: minuteFormatter, ampm: ampmFormatter)
    }()
    
    static var splitDateFormatter: (date: DateFormatter,dateWithDay: DateFormatter, time: DateFormatter, ampm: DateFormatter,todayDate:DateFormatter,day:DateFormatter,month:DateFormatter) = {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM d, yyyy"
        
        let dateWithDayFormatter = DateFormatter()
        dateWithDayFormatter.dateFormat = "MMM d, yyyy, EEEE"
        
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "h:mm,a"
        let ampmFormatter = DateFormatter()
        ampmFormatter.dateFormat = "a"
        
        let dayFormatter = DateFormatter()
        dayFormatter.dateFormat = "EEEE"
        
        let todayDateFormatter = DateFormatter()
        todayDateFormatter.dateFormat = "d"
        
        let monthFormatter = DateFormatter()
        monthFormatter.dateFormat = "MMMM yyyy"
        
        return (date: dateFormatter,dateWithDay: dateWithDayFormatter, time: timeFormatter, ampm: ampmFormatter , todayDate:todayDateFormatter , day:dayFormatter,month:monthFormatter)
    }()
}

extension Date {
    
    var timeString: String {
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "h:mm a"
        return timeFormatter.string(from: self)
    }
    
    /// Returns `Date` for user-defined `hour` and `minute` components from another date.
    ///
    /// - note: The components from `date` parameter are used to return `Date` object. Exceptoions: `hour`, `minute`, and `second`.
    /// - parameter hour: Hour to use.
    /// - parameter minute: Minute to use.
    /// - parameter date: The `Date` to use.
    /// - returns: The `Date` of the specified parameters.
    static func date(hour: Int, minute: Int, from date: Date) -> Date {
        let calendar = Calendar(identifier: .gregorian)
        let cmpts: Set<Calendar.Component> = [.year, .month, .day, .hour, .minute, .second]
        var components = calendar.dateComponents(cmpts, from: date)
        components.hour = hour
        components.minute = minute
        components.second = 0
        return calendar.date(from: components) ?? Date()
    }
}

