//
//  MenuViewController.swift
//  AKSwiftSlideMenu
//
//  Created by Ashish on 21/09/15.
//  Copyright (c) 2015 Kode. All rights reserved.
//

import UIKit
import UserNotifications

protocol SlideMenuDelegate {
    func slideMenuItemSelectedAtIndex(_ index : Int32)
}

class MenuViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    
    let center = UNUserNotificationCenter.current()
    
    /**
    *  Array to display menu options
    */
    @IBOutlet var tblMenuOptions : UITableView!
    
    /**
    *  Transparent button to hide menu
    */
    @IBOutlet var btnCloseMenuOverlay : UIButton!
    
    /**
    *  Array containing menu options
    */
    var arrayMenuOptions = [Dictionary<String,String>]()
    var secondarrayMenuOptions = [Dictionary<String,String>]()
    
    /**
    *  Menu button which was tapped to display the menu
    */
    var btnMenu : UIButton!
    
    /**
    *  Delegate of the MenuVC
    */
    var delegate : SlideMenuDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblMenuOptions.tableFooterView = UIView()
        
        
        print("(UserDefaults.standard.integer(forKey:)) in menu",(UserDefaults.standard.integer(forKey: "selectedIndex")))
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateArrayMenuOptions()
        
        selectedDivision()
    }
    
    func setIndexToRow(_ index: Int) {
        
        let selectedCellIndexPath = IndexPath(row: index, section: 0)
        //  [self tableView:self.drawerView.drawerTable didSelectRowAtIndexPath:selectedCellIndexPath];
       
        print("selectedCellIndexPath ",selectedCellIndexPath)
        
        tblMenuOptions.selectRow(at: selectedCellIndexPath, animated: true, scrollPosition: .middle)
        
    }
    //nav_division_barishal  nav_division_khulna  nav_division_mym nav_division_rajshahi nav_dision_rangpur nav_division_sylhet
    func updateArrayMenuOptions(){
        arrayMenuOptions.append(["title":"Dhaka", "icon":"nav_division_dhaka"])
        arrayMenuOptions.append(["title":"Chittagong", "icon":"nav_division_ctg"])
        arrayMenuOptions.append(["title":"Sylhet", "icon":"nav_division_sylhet"])
        arrayMenuOptions.append(["title":"Rajshahi", "icon":"nav_division_rajshahi"])
        arrayMenuOptions.append(["title":"Barisal", "icon":"nav_division_barishal"])
        arrayMenuOptions.append(["title":"Khulna", "icon":"nav_division_khulna"])
        arrayMenuOptions.append(["title":"Mymensingh", "icon":"nav_division_mym"])
        arrayMenuOptions.append(["title":"Rangpur", "icon":"nav_dision_rangpur"])
        
        
        
        secondarrayMenuOptions.append(["title":"Rate This App", "icon":"nav_icon_rate"])
        secondarrayMenuOptions.append(["title":"Share This App", "icon":"nav_icon_share"])
        secondarrayMenuOptions.append(["title":"About Us", "icon":"nav_icon_about_us"])
        
        
        
        tblMenuOptions.reloadData()
    }
    
   func selectedDivision(){
    
//    UserDefaults.standard.set(arrayMenuOptions[(UserDefaults.standard.integer(forKey: "selectedIndex"))]["title"], forKey: "selectedDivision")
//
//    print("division........", (UserDefaults.standard.string(forKey: "selectedDivision"))!)
    
    
    }
    
    @IBAction func onCloseMenuClick(_ button:UIButton!){
        btnMenu.tag = 0
        
        if (self.delegate != nil) {
            var index = Int32(button.tag)
            
            print("index  on close menu click",index)
           
            if(button == self.btnCloseMenuOverlay){
                index = -1
            }
           // delegate?.slideMenuItemSelectedAtIndex(index)
        }
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.view.frame = CGRect(x: -UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width,height: UIScreen.main.bounds.size.height)
            self.view.layoutIfNeeded()
            self.view.backgroundColor = UIColor.clear
            }, completion: { (finished) -> Void in
                self.view.removeFromSuperview()
                self.removeFromParentViewController()
        })
    }
    
    
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cellMenu")!
        
        
        
//        cell.selectionStyle = UITableViewCellSelectionStyle.gray
//        cell.layoutMargins = UIEdgeInsets.zero
//        cell.preservesSuperviewLayoutMargins = false
//        cell.backgroundColor = UIColor.clear
        
        let lblTitle : UILabel = cell.contentView.viewWithTag(101) as! UILabel
        let imgIcon : UIImageView = cell.contentView.viewWithTag(100) as! UIImageView
        
        
        if indexPath.section == 0{
            
            imgIcon.image = UIImage(named: arrayMenuOptions[indexPath.row]["icon"]!)
            lblTitle.text = arrayMenuOptions[indexPath.row]["title"]!
            
        }else{
            
            imgIcon.image = UIImage(named: secondarrayMenuOptions[indexPath.row]["icon"]!)
            lblTitle.text = secondarrayMenuOptions[indexPath.row]["title"]!
        }
        
        let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor.lightText
        cell.selectedBackgroundView = backgroundView

        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let btn = UIButton(type: UIButtonType.custom)
        btn.tag = indexPath.row
        self.onCloseMenuClick(btn)
        
//        let selectedCell:UITableViewCell = tableView.cellForRow(at: indexPath)!
//        selectedCell.contentView.backgroundColor = UIColor.red
        
        if indexPath.section == 0 {
            
            if indexPath.row == (UserDefaults.standard.integer(forKey: "selectedIndex")){
                
                print("same row")
                
            }else{
                
                UserDefaults.standard.set(indexPath.row, forKey: "selectedIndex")
                
                UserDefaults.standard.set(arrayMenuOptions[(UserDefaults.standard.integer(forKey: "selectedIndex"))]["title"], forKey: "selectedDivision")
                
                UserDefaults.standard.set(arrayMenuOptions[(UserDefaults.standard.integer(forKey: "selectedIndex"))]["title"], forKey: "selectedDistrict")
                
                print("division........", (UserDefaults.standard.string(forKey: "selectedDivision"))!)
                print("selectedIndex........", (UserDefaults.standard.string(forKey: "selectedIndex"))!)
                
//                center.removeAllPendingNotificationRequests()
//                center.removeAllDeliveredNotifications()
                
                let mainMenu =  MainViewController()
                
               
//                mainMenu.findDistrictList()
//                mainMenu.calculateRamadanStartDate()
                mainMenu.parseDivisionData()
                // mainMenu.todaysPrayerTime()
//                mainMenu.parseRamadanData()
                
//                mainMenu.setAlarmData()
//                mainMenu.setRamadanAlarm()
                
                delegate?.slideMenuItemSelectedAtIndex(Int32(indexPath.row))

            }
        }else{
            
            if indexPath.row == 0 {
                
                print("rate this app ")
                
                let urlStr = "itms-apps://itunes.apple.com/us/app/finix-ramadan-calendar/id1384684444?ls=1&mt=8"
                UIApplication.shared.open(URL(string: urlStr)!, options: [:], completionHandler: nil)

//                UIApplication.shared.openURL(URL(string: "http://appstore.com/keynote")!)
//
             
                
            }else if indexPath.row == 1{
                
                print("share this app ")
               //find top viewcontroller
                let topViewController : UIViewController = self.navigationController!.topViewController!
                
                
                // link
                let text = "https://itunes.apple.com/us/app/finix-ramadan-calendar/id1384684444?ls=1&mt=8"
                
//                // set up activity view controller
                let textToShare = [ text ]
                let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash


                // present the view controller
                topViewController.present(activityViewController, animated: true, completion: nil)
                
                

                
            }else if indexPath.row == 2{
                
                print("about us ")
                
                let destViewController : UIViewController = self.storyboard!.instantiateViewController(withIdentifier: "AboutUsViewController")
                
                self.navigationController!.pushViewController(destViewController, animated: true)
                
            }
            
        }
        
       
    }
    
//    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
//        
//        let cellToDeSelect:UITableViewCell = tableView.cellForRow(at: indexPath as IndexPath)!
//        cellToDeSelect.contentView.backgroundColor = UIColor.clear
//        
//    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            
            return arrayMenuOptions.count

        }else{
            
            return secondarrayMenuOptions.count
        }
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2 ;
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "headerCell") as! UITableViewCell
        
        let lblTitle : UILabel = cell.contentView.viewWithTag(102) as! UILabel

        if section == 0{
            
            lblTitle.text = "Select Your Preferred Division"
            
        }else{
            
            lblTitle.text = "Communication"
            
            
        }
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 40
    }

 


}
