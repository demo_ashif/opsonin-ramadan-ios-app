//
//  PrayerTime.swift
//  Finix Ramadan Calendar
//
//  Created by Md.Ballal Hossen on 25/4/18.
//  Copyright © 2018 Sujan. All rights reserved.
//

import UIKit

class PrayerTime: NSObject {
    
    var day:String!
    var month:String!
    var sunrise:String!
    var fajr:String!
    var zuhr:String!
    var asr:String!
    var magrib:String!
    var isha:String!
    
//    init(day:String,month:String,sunrise:String,fajr:String,zuhr:String,asr:String,monasrth:String,magrib:String,isha:String) {
//
//        self.day = day
//        self.month = month
//        self.sunrise = sunrise
//        self.fajr = fajr
//        self.zuhr = zuhr
//        self.asr = asr
//        self.magrib = magrib
//        self.isha = isha
//    }

}
