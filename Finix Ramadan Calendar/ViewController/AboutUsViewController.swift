//
//  AboutUsViewController.swift
//  Finix Ramadan Calendar
//
//  Created by Md.Ballal Hossen on 1/5/18.
//  Copyright © 2018 Sujan. All rights reserved.
//

import UIKit

class AboutUsViewController: UIViewController {

    
    
    
    @IBOutlet weak var websiteLinkButton: UIButton!
    
    @IBOutlet weak var fbLinkButton: UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "About Us"
        
       

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    

    
    @IBAction func websiteLinkButtonAction(_ sender: Any) {
        
        UIApplication.shared.openURL(URL(string: "http://www.opsonin-pharma.com")!)
    }
    
    @IBAction func fbLinkButtonAction(_ sender: Any) {
        
        UIApplication.shared.openURL(URL(string: "https://www.facebook.com/OpsoninPharma")!)
    }
    

}
