//
//  AlQuranViewController.swift
//  Finix Ramadan Calendar
//
//  Created by Md.Ballal Hossen on 22/5/18.
//  Copyright © 2018 Sujan. All rights reserved.
//

import UIKit

class AlQuranViewController: UIViewController {

    
    var suraListArray = NSArray()
    
    @IBOutlet weak var audioButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Al-Quran"
        
        audioButton.titleLabel?.lineBreakMode = .byWordWrapping
        audioButton.titleLabel?.numberOfLines = 2
        DispatchQueue.global(qos: .userInitiated).async {
            
            self.readJSON()
            
            // Bounce back to the main thread to update the UI
            DispatchQueue.main.async {
                
                //print("main thread")
                
                
            }
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func readJSON(){
        
        if let path = Bundle.main.path(forResource: "quran_all_surah_list", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves) as? NSDictionary
                
                
                
                suraListArray = jsonResult?.value(forKeyPath: "chapters") as! NSArray
                
                
                
                
            } catch {
                // handle error
            }
        }
        
    }


    @IBAction func meaningButtonAction(_ sender: Any) {
        
        
       
        let destViewController = storyboard?.instantiateViewController(withIdentifier: "QuranViewController") as! QuranViewController
        
        destViewController.suraList = suraListArray
        
        self.navigationController!.pushViewController(destViewController, animated: true)

    }
    
    @IBAction func audioButtonAction(_ sender: Any) {
        
        let destViewController = storyboard?.instantiateViewController(withIdentifier: "AudioViewController") as! AudioViewController
        
       // destViewController.allSuraListArray = suraListArray
        
        self.navigationController!.pushViewController(destViewController, animated: true)

    }
    
}
