//
//  AllahNameViewController.swift
//  Finix Ramadan Calendar
//
//  Created by Md.Ballal Hossen on 23/4/18.
//  Copyright © 2018 Sujan. All rights reserved.
//

import UIKit

class ZakatCalculatorViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    

    

    @IBOutlet weak var nameTableView: UITableView!
    
     var allah_99_names:NSArray = NSArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        nameTableView.delegate = self
        nameTableView.dataSource = self
        
        nameTableView.rowHeight = UITableViewAutomaticDimension
        nameTableView.estimatedRowHeight = 170

        readJsonAndPopulateView()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func readJsonAndPopulateView() {
        
       
        
        
        if let path = Bundle.main.path(forResource: "allah_99_names", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves) as? NSDictionary
                
                print("json",jsonResult!)
                
                allah_99_names = jsonResult?.value(forKeyPath: "allah_99_names") as! NSArray
                
                
            } catch {
                // handle error
            }
        }
        
        
        print("allah_99_names",allah_99_names)
        
        
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return allah_99_names.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell", for: indexPath) as! NameTableViewCell
        
        cell.contentView.layer.cornerRadius = 5
        cell.contentView.clipsToBounds = true
        
        cell.arabicLabel.text = (allah_99_names[indexPath.row] as AnyObject).value(forKey: "name_arabic") as? String
        cell.englishLabel.text = (allah_99_names[indexPath.row] as AnyObject).value(forKey: "name_english") as? String
        cell.meaningLabel.text = (allah_99_names[indexPath.row] as AnyObject).value(forKey: "meaning") as? String
        cell.explanationLabel.text = (allah_99_names[indexPath.row] as AnyObject).value(forKey: "explanation") as? String
        
        return cell
        
    }

}
