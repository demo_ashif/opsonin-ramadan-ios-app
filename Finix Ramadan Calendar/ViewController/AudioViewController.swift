//
//  AudioViewController.swift
//  Finix Ramadan Calendar
//
//  Created by Md.Ballal Hossen on 22/5/18.
//  Copyright © 2018 Sujan. All rights reserved.
//

import UIKit

class AudioViewController: UIViewController {

    var allSuraListArray = NSArray()

    var suraAudioList = [String]()
    var downloadAudioList = [String]()

   // let downloadService = DownloadService()


    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Audio"
        
        

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        readJSON()
        suraAudioList.removeAll()
        downloadAudioList.removeAll()
        makeAudioList()
        
        // let downloadService = DownloadService()
    }
    
    func readJSON(){
        
        if let path = Bundle.main.path(forResource: "quran_all_surah_list", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves) as? NSDictionary
                
                
                
                allSuraListArray = jsonResult?.value(forKeyPath: "chapters") as! NSArray
                
                
                
                
            } catch {
                // handle error
            }
        }
        
    }

    
    
    func makeAudioList(){
        
        
        for i in 0 ..< allSuraListArray.count{
            
            let fileName = (String(format: "%03d", i + 1))
            
            let fileNameForSearch = fileName + ".mp3"
            
            if Bundle.main.path(forResource: fileNameForSearch, ofType:nil ) != nil
            {
                
                suraAudioList.append(fileName)
                
            }else{
                
                let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
                let url = NSURL(fileURLWithPath: path)
                if let pathComponent = url.appendingPathComponent(fileNameForSearch) {
                    let filePath = pathComponent.path
                    //print("filePath.......",filePath)
                    let fileManager = FileManager.default
                    
                    if fileManager.fileExists(atPath: filePath) {
                        
                        print("FILE AVAILABLE in document")
                        
                        suraAudioList.append(fileName)
                        
                    }else{
                        
                        downloadAudioList.append(fileName)
                    }
                }
            }
            
        }
        
        print("suraAudioList",suraAudioList.count)
        
    }

    
    @IBAction func downloadButtonAction(_ sender: Any) {
        
        if downloadAudioList.count > 0 {
            
            let destViewController = storyboard?.instantiateViewController(withIdentifier: "DownloadViewController") as! DownloadViewController
            
            destViewController.downloadSuraList = downloadAudioList
            
            self.navigationController!.pushViewController(destViewController, animated: true)

        }else{
            
            let alert = UIAlertController(title: "", message: "No Surah left for download.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)

        }
        
    }
    
    @IBAction func playAllButtonAction(_ sender: Any) {
        
        let destViewController = storyboard?.instantiateViewController(withIdentifier: "PlayerViewController") as! PlayerViewController
        
       destViewController.suraList = suraAudioList
        
        self.navigationController!.pushViewController(destViewController, animated: true)

    }
    
}
