//
//  CompassViewController.swift
//  Finix Ramadan Calendar
//
//  Created by Md.Ballal Hossen on 20/4/18.
//  Copyright © 2018 Sujan. All rights reserved.
//

import UIKit
import CoreLocation

class CompassViewController: BaseViewController {

    @IBOutlet weak var compassImageView: UIImageView!
    
    let locationDelegate = LocationDelegate()
    var latestLocation: CLLocation? = nil
    var yourLocationBearing: CGFloat { return latestLocation?.bearingToLocationRadian(self.yourLocation) ?? 0 }
    var yourLocation: CLLocation {
        get { return UserDefaults.standard.currentLocation }
        set { UserDefaults.standard.currentLocation = newValue }
    }
    
    let locationManager: CLLocationManager = {
        $0.requestWhenInUseAuthorization()
        $0.desiredAccuracy = kCLLocationAccuracyBest
        $0.startUpdatingLocation()
        $0.startUpdatingHeading()
        return $0
    }(CLLocationManager())
    
    private func orientationAdjustment() -> CGFloat {
        let isFaceDown: Bool = {
            switch UIDevice.current.orientation {
            case .faceDown: return true
            default: return false
            }
        }()
        
        let adjAngle: CGFloat = {
            switch UIApplication.shared.statusBarOrientation {
            case .landscapeLeft:  return 90
            case .landscapeRight: return -90
            case .portrait, .unknown: return 0
            case .portraitUpsideDown: return isFaceDown ? 180 : -180
            }
        }()
        return adjAngle
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // addSlideMenuButton()
        
        
               self.title = "Compass"

                print("setHeading")
                
                locationManager.delegate = locationDelegate
                
                locationDelegate.locationCallback = { location in
                    self.latestLocation = location
                }
                
                locationDelegate.headingCallback = { newHeading in
                    
                    func computeNewAngle(with newAngle: CGFloat) -> CGFloat {
                        let heading: CGFloat = {
                            let originalHeading = self.yourLocationBearing - newAngle.degreesToRadians
                            switch UIDevice.current.orientation {
                            case .faceDown: return -originalHeading
                            default: return originalHeading
                            }
                        }()
                        
                        return CGFloat(self.orientationAdjustment().degreesToRadians + heading)
                    }
                    
                    UIView.animate(withDuration: 0.5) {
                        let angle = computeNewAngle(with: CGFloat(newHeading))
                        self.compassImageView.transform = CGAffineTransform(rotationAngle: angle)
                    }
                }
                
        
        
        //perform(#selector(setHeading), with: nil, afterDelay: 0.1)

        
    }
    
    
    
    

}
