//
//  DownloadViewController.swift
//  Finix Ramadan Calendar
//
//  Created by Md.Ballal Hossen on 22/5/18.
//  Copyright © 2018 Sujan. All rights reserved.
//

import UIKit

class DownloadViewController: UIViewController,UITableViewDelegate,TrackCellDelegate,UITableViewDataSource,URLSessionDownloadDelegate,URLSessionDelegate{
    
    

    
    
    @IBOutlet weak var tableView: UITableView!
    
    var downloadSuraList = [String]()
    var nameDic = NSDictionary()

    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    lazy var downloadService = appDelegate.downloadService
    
    // Create downloadsSession here, to set self as delegate
    lazy var downloadsSession: URLSession = {
        //    let configuration = URLSessionConfiguration.default
        let configuration = URLSessionConfiguration.background(withIdentifier: "bgSessionConfiguration")
        return URLSession(configuration: configuration, delegate: self , delegateQueue: nil)
    }()
    
    // Get local file path: download task stores tune here; AV player plays it.
    let documentsPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    func localFilePath(for url: URL) -> URL {
        return documentsPath.appendingPathComponent(url.lastPathComponent)
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Surah Download"
        
        tableView.delegate = self
        tableView.dataSource = self
        
        print("downloadSuraList count",downloadSuraList.count)
        
        //sets the downloadsSession property of DownloadService.
        downloadService.downloadsSession = downloadsSession

        readNameJSON()
    }
    override func viewDidAppear(_ animated: Bool) {
        
        
        
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func readNameJSON(){
        
        if let path = Bundle.main.path(forResource: "quran_all_surah_name", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves) as? NSDictionary
                
                
                
                nameDic = jsonResult!
                
                // print("nameDic.....",nameDic)
                
                
                
            } catch {
                // handle error
            }
        }
        
    }


    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return downloadSuraList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //let cell: TrackCell = tableView.dequeueReusableCell(for: indexPath)
        let cell : TrackCell = tableView.dequeueReusableCell(withIdentifier: "TrackCell") as! TrackCell
        
        // Delegate cell button tap events to this view controller
        cell.delegate = self
        
        let surakey = downloadSuraList[indexPath.row]
        let suraKeyInt = Int(surakey)
        let name = nameDic.value(forKey: surakey) as? String
        
        let fileName = (String(format: "%03d.mp3", suraKeyInt!))
        
        let urlString = "https://download.quranicaudio.com/quran/abdurrahmaan_as-sudays/" + fileName
        
        
        let url = URL(string:urlString)!
       

         
        let track = Track(name: name!, previewURL: url, index: indexPath.row)
        
       // cell.updateDisplay(progress: download.progress, totalSize: totalSize)
        
        cell.configure(track: track, downloaded: track.downloaded, download: downloadService.activeDownloads[track.previewURL])
        
       // cell.setNeedsDisplay()
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 62.0
    }
    
    //download functionality
    
    func downloadTapped(_ cell: TrackCell) {
        
        print("download button clicked")
       let reachability = Reachability()!
        
       if reachability.connection != .none{
        
         print("internet")
        
          if let indexPath = tableView.indexPath(for: cell) {
            
            let surakey = downloadSuraList[indexPath.row]
            
            let suraKeyInt : Int? = Int(surakey)
            
            let name = nameDic.value(forKey: surakey) as? String
            
            let fileName = (String(format: "%03d.mp3", suraKeyInt!))
            
            let urlString = "https://download.quranicaudio.com/quran/abdurrahmaan_as-sudays/\(fileName)"
            
            let url  = URL(string: urlString)!
            
            let track = Track(name: name!, previewURL: url, index: indexPath.row)
                
            print("url in download button click",track.previewURL)
                
            downloadService.startDownload(track)
                
            reload(indexPath.row)
   
        }
       }else{
        
                    print("no internet")
        
                    let alertController = UIAlertController (title: "No Internet", message: "Go to Settings?", preferredStyle: .alert)
        
                    let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
                        guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                            return
                        }
        
                        if UIApplication.shared.canOpenURL(settingsUrl) {
                            UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                                print("Settings opened: \(success)") // Prints true
                            })
                        }
                    }
                    alertController.addAction(settingsAction)
                    let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
                    alertController.addAction(cancelAction)
        
                    present(alertController, animated: true, completion: nil)

        }
        
    }

    
    func pauseTapped(_ cell: TrackCell) {
        
        print("pause button clicked")
        
        if let indexPath = tableView.indexPath(for: cell) {
            
            let surakey = downloadSuraList[indexPath.row]
            
            let suraKeyInt : Int? = Int(surakey)
            
            let name = nameDic.value(forKey: surakey) as? String
            
            let fileName = (String(format: "%03d.mp3", suraKeyInt!))
            
            let urlString = "https://download.quranicaudio.com/quran/abdurrahmaan_as-sudays/\(fileName)"
            
            // let encodedHost = urlString.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
            
            let url  = URL(string: urlString)!
            
            let track = Track(name: name!, previewURL: url, index: indexPath.row)

            downloadService.pauseDownload(track)
            reload(indexPath.row)
        }
    }
    
    func resumeTapped(_ cell: TrackCell) {
        
        print("resume button clicked")
        
    }
    
    func cancelTapped(_ cell: TrackCell) {
        
        print("cancel button clicked")
        
        if let indexPath = tableView.indexPath(for: cell) {
            
            let surakey = downloadSuraList[indexPath.row]
            
            let suraKeyInt : Int? = Int(surakey)
            
            let name = nameDic.value(forKey: surakey) as? String
            
            let fileName = (String(format: "%03d.mp3", suraKeyInt!))
            
            let urlString = "https://download.quranicaudio.com/quran/abdurrahmaan_as-sudays/\(fileName)"
            
            // let encodedHost = urlString.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
            
            let url  = URL(string: urlString)!
            
            let track = Track(name: name!, previewURL: url, index: indexPath.row)
            
            downloadService.cancelDownload(track)
            reload(indexPath.row)
        }

        
    }
    

    

    
    // Stores downloaded file
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        // 1
        guard let sourceURL = downloadTask.originalRequest?.url else { return }
        let download = downloadService.activeDownloads[sourceURL]
        downloadService.activeDownloads[sourceURL] = nil
        // 2
        let destinationURL = localFilePath(for: sourceURL)
        
        print(destinationURL)
        // 3
        let fileManager = FileManager.default
        try? fileManager.removeItem(at: destinationURL)
        do {
            try fileManager.copyItem(at: location, to: destinationURL)
            download?.track.downloaded = true
            
            print("saved in data",download?.track.index as Any)
            
            if let index = download?.track.index {
                DispatchQueue.main.async {
                    //self.tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .none)
                    
                    
                    self.downloadSuraList.remove(at: index)
                    self.tableView.reloadData()
                    
                    print("reload...",self.downloadSuraList.count)
                }
            }
            
        } catch let error {
            print("Could not copy file to disk: \(error.localizedDescription)")
        }
        // 4
//        if let index = download?.track.index {
//            DispatchQueue.main.async {
//                //self.tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .none)
//
//                self.downloadSuraList.remove(at: index)
//                self.tableView.reloadData()
//            }
//        }
    }
    
    // Updates progress info
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask,
                    didWriteData bytesWritten: Int64, totalBytesWritten: Int64,
                    totalBytesExpectedToWrite: Int64) {
        
       // print("totalBytesWritten")
        // 1
        guard let url = downloadTask.originalRequest?.url,
            
            let download = downloadService.activeDownloads[url]  else { return }
        
        
        // 2
        download.progress = Float(totalBytesWritten) / Float(totalBytesExpectedToWrite)
        
        
        // 3
        let totalSize = ByteCountFormatter.string(fromByteCount: totalBytesExpectedToWrite,
                                                  countStyle: .file)
        // 4
        DispatchQueue.main.async {
            
            print("active download print main queue",download.progress)
            
            if let trackCell = self.tableView.cellForRow(at: IndexPath(row: download.track.index,
                                                                       section: 0)) as? TrackCell {
                
                print("track cell")
                trackCell.updateDisplay(progress: download.progress, totalSize: totalSize)
                
            }
        }
    }
    
    // Standard background session handler
    func urlSessionDidFinishEvents(forBackgroundURLSession session: URLSession) {
        
        DispatchQueue.main.async {
            if let appDelegate = UIApplication.shared.delegate as? AppDelegate,
                let completionHandler = appDelegate.backgroundSessionCompletionHandler {
                appDelegate.backgroundSessionCompletionHandler = nil
                completionHandler()
            }
        }
    }


    
    // Update track cell's buttons
    func reload(_ row: Int) {
        
        tableView.reloadRows(at: [IndexPath(row: row, section: 0)], with: .none)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // check if the back button was pressed
        if isMovingFromParentViewController {
            
            print("back",downloadService.downloadUrl as Any)
            print("Back button was pressed.")
        }
    }

}
