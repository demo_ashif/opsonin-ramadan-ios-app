//
//  DuaViewController.swift
//  Finix Ramadan Calendar
//
//  Created by Md.Ballal Hossen on 20/4/18.
//  Copyright © 2018 Sujan. All rights reserved.
//

import UIKit


class DuaViewController: BaseViewController {
    
    @IBOutlet weak var sehriArabicLabel: UILabel!
    @IBOutlet weak var sehriPronunLabel: UILabel!
    @IBOutlet weak var sehriBanglaLabel: UILabel!
    
    @IBOutlet weak var ifterArabicLabel: UILabel!
    @IBOutlet weak var ifterPronunLabel: UILabel!
    @IBOutlet weak var ifterBanglaLabel: UILabel!
    
   
    
  //  var audioPlayer = AVAudioPlayer()
    
    override func viewDidLoad() {
        super.viewDidLoad()

       self.title = "Ramadan Dua"
        
        readJsonAndPopulateView()
        
        print("viewdidload")

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    func readJsonAndPopulateView() {
        
        print("readJsonAndPopulateView")
        
        var sehri_iftar_dua:NSDictionary = NSDictionary()
        
        
        if let path = Bundle.main.path(forResource: "sehri_iftar_dua", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves) as? NSDictionary
                
                print("json",jsonResult!)
                
                sehri_iftar_dua = jsonResult?.value(forKeyPath: "sehri_iftar_dua") as! NSDictionary
                
                
            } catch {
                // handle error
            }
        }
        
        
        print("sehri_iftar_dua",sehri_iftar_dua)
        
        sehriArabicLabel.text = sehri_iftar_dua.value(forKeyPath: "sehri_niot") as? String
        sehriPronunLabel.text = sehri_iftar_dua.value(forKeyPath: "sehri_niot_pronun") as? String
        sehriBanglaLabel.text = sehri_iftar_dua.value(forKeyPath: "sehri_niot_bangla_meaning") as? String

        ifterArabicLabel.text = sehri_iftar_dua.value(forKeyPath: "iftar_dua") as? String
        ifterPronunLabel.text = sehri_iftar_dua.value(forKeyPath: "iftar_dua_pronun") as? String
        ifterBanglaLabel.text = sehri_iftar_dua.value(forKeyPath: "iftar_dua_bangla_meaning") as? String

        
    }
    
    @IBAction func playSehriButtonAction(_ sender: Any) {
        
        print("play")

    }
    
    @IBAction func pauseSehriButtonAction(_ sender: Any) {
        
      //  audioPlayer.pause()
    }
    
    @IBAction func playIfterButtonAction(_ sender: Any) {

        
    }
    
    @IBAction func pauseIfterButtonAction(_ sender: Any) {
        
       // audioPlayer.pause()
    }
    
}
