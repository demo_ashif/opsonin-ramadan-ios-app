//
//  FirstScreenViewController.swift
//  Finix Ramadan Calendar
//
//  Created by Md.Ballal Hossen on 1/5/18.
//  Copyright © 2018 Sujan. All rights reserved.
//

import UIKit

class FirstScreenViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        
        let tapOnView = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        
        self.view.addGestureRecognizer(tapOnView)
        
        

        UserDefaults.standard.set(18, forKey: "ramadanStartDate")
         
    
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
            
            self.performSegue(withIdentifier: "leadingToMain", sender: self)
            
            print("3 sec")
        })

    }
    
    func getRamadanStartDate()
    {
        let myUrl = NSURL(string: "https://finix-ramadan-backend.herokuapp.com/feed");
        
        let request = NSMutableURLRequest(url:myUrl! as URL);
        
        request.httpMethod = "GET"
        
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            
            data, response, error in
            
            
            if error != nil
            {
                print("error=\(String(describing: error))")
                return
            }
            
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            //  print("responseString = \(String(describing: responseString))")
            
            
            // Convert server json response to NSDictionary
            do {
                if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    
                    
                    let startDate =  convertedJsonIntoDict.value(forKey: "ramadan_start") as! Int
                    
                    
                    
                    if startDate == UserDefaults.standard.integer(forKey: "ramadanStartDate"){
                        
                        print(" same startDate")
                        
                    }else{
                        
                        print("not same startDate")
                        
                        
                         UserDefaults.standard.set(startDate, forKey: "ramadanStartDate")
                    
                    }
                    
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
            
        }
        
        task.resume()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonClickAction(_ sender: Any) {
        
//        let destViewController : UIViewController = self.storyboard!.instantiateViewController(withIdentifier: "MainViewController")
//
//        self.present(destViewController, animated: true, completion: nil)
        
        
        
    }
    
   @objc func handleTap() {
    
    //print("tap on view")
    
//    let destViewController : UIViewController = self.storyboard!.instantiateViewController(withIdentifier: "MainViewController")
//    
//    self.navigationController!.pushViewController(destViewController, animated: true)
        
    }

}
