//
//  MainViewController.swift
//  Finix Ramadan Calendar
//
//  Created by Md.Ballal Hossen on 22/4/18.
//  Copyright © 2018 Sujan. All rights reserved.
//

import UIKit
import UserNotifications


class MainViewController: BaseViewController,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    let center = UNUserNotificationCenter.current()
    
    @IBOutlet weak var ramadanTimeView: UIView!
    
    @IBOutlet weak var sehriTimeLabel: UILabel!
    
    @IBOutlet weak var ifterTimelabel: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var ramadanNoLabel: UILabel!
    
    @IBOutlet weak var timerView: UIView!
    
    @IBOutlet weak var counterLabel: UILabel!
    
    
    @IBOutlet weak var menuCollectionView: UICollectionView!
    
    @IBOutlet weak var collectionViewHeight: NSLayoutConstraint!
    
    var arrayMenu = [Dictionary<String,String>]()
    var prayerArray : NSArray!
    var prayerAlarmArray : NSArray!
    var ramadanArray : NSArray!
    var ramadanstartDate : Date!
    var districtArray =  NSArray()

    var add_iftar_mins : Int = 0
    var add_sehri_mins : Int = 0
    var minus_iftar_mins : Int = 0
    var minus_sehri_mins : Int = 0

    
    override func viewDidLoad() {
        super.viewDidLoad()
        


        print("start sate in main view",UserDefaults.standard.integer(forKey: "ramadanStartDate"))
        
        UserDefaults.standard.set(0, forKey: "isFromSetting")
        
        let logo = UIImage(named: "toolbar_logo.png")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
        
        
       // ramadanTimeView.isHidden = true
        timerView.isHidden = true
        
        addSlideMenuButton()
        
        menuCollectionView.delegate = self
        menuCollectionView.dataSource = self
        
        arrayMenu.append(["title":"Ramadan Time", "icon":"menu_icon_calendar"])
        arrayMenu.append(["title":"Al-Quran", "icon":"menu_icon_quran"])
        arrayMenu.append(["title":"Prayer Times", "icon":"menu_icon_prayer_time"])
        arrayMenu.append(["title":"Zakat Calculator", "icon":"menu_icon_zakat"])
        arrayMenu.append(["title":"Ramadan Dua", "icon":"menu_icon_dua"])
        arrayMenu.append(["title":"Qibla Compass", "icon":"menu_icon_qibla"])
        arrayMenu.append(["title":"Tasbih Counter", "icon":"menu_icon_tasbih"])
        arrayMenu.append(["title":"Alarm Setting", "icon":"menu_icon_settings"])
        
//        getRamadanStartDate()
        
        calculateRamadanStartDate()
        
        //parseDivisionData()
        
        center.removeAllPendingNotificationRequests()
        center.removeAllDeliveredNotifications()
        

        center.getPendingNotificationRequests(completionHandler: { requests in
            print("Pending Request: ",requests)
            for request in requests {
                print(" Request: ",request)
            }
        })
        
        parseDivisionData()
        setAlarmData()
        
        findDistrictList()
        parseRamadanData()
        setRamadanAlarm()
        
        populateRamadanView()
        
      
        // Repeat "func Update() " every second and update the label
        
        Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(UpdateTime), userInfo: nil, repeats: true)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        print("........",UserDefaults.standard.integer(forKey: "isFromSetting"))
        
        if ( UserDefaults.standard.integer(forKey: "isFromSetting") == 1 ){
            
            UserDefaults.standard.set(0, forKey: "isFromSetting")
            
            print("from setting")
            
            findDistrictList()
            parseRamadanData()

            populateRamadanView()

            
        }
        
        
        
    }
   

    func calculateRamadanStartDate()
    {
        
        let calendar = NSCalendar.current
        
        //UserDefaults.standard.integer(forKey: "ramadanStartDate")
        //ramadan date
        var ramadanDateComponents = DateComponents()
        ramadanDateComponents.year = 2018
        ramadanDateComponents.month = 5
        ramadanDateComponents.day = UserDefaults.standard.integer(forKey: "ramadanStartDate")
        ramadanDateComponents.hour = 3
        ramadanDateComponents.minute = 46

        
        ramadanstartDate = calendar.date(from: ramadanDateComponents)! as Date
        
        print("ramadanstartDate",ramadanstartDate)

    }
    
    func getCurrentDateData(complitionHandler handler: (_ index: Int) -> Void)
    {
     
        let currentDateComp = getCurrentDateComponents()
        
        for i in 0 ..< prayerArray.count {
            
            let singleDay = prayerArray[i] as! NSDictionary
            
            let dayNo =  singleDay.value(forKey: "day_no") as! NSString
            let monthNo = singleDay.value(forKey: "month_no") as! NSString
            
            if monthNo.integerValue == currentDateComp.month && dayNo.integerValue == currentDateComp.day
            {
                handler(i)
                break
            }
        }
       
        
    }
    
    func populateRamadanView(){
        
        //ramadan date
        var dateComponents = DateComponents()
        dateComponents.year = 2018
        dateComponents.month = 5
        dateComponents.day = UserDefaults.standard.integer(forKey: "ramadanStartDate")
        dateComponents.hour = 3
        dateComponents.minute = 46
        
        
        
        let currentDate = Date()
        
        let calendar = NSCalendar.current
        
        let formatter = DateFormatter.splitDateFormatter
        
        let ramadanstart = calendar.date(from: dateComponents)! as Date
        

        
        if currentDate >= ramadanstart {
            
            
            
            for i in 0 ..< 30 {
                
                let singleRamadan = ramadanArray[i] as! NSDictionary
                
                let ramadanNo =  singleRamadan.value(forKey: "ramadan_no")
                
                let ramadanDate = Calendar.current.date(byAdding: .day, value: (ramadanNo as! Int - 1), to: ramadanstartDate as Date)
                
                var ramadanDateComps = Calendar.current.dateComponents([.year,.month,.day,.hour,.minute,.second], from: ramadanDate!)
                
                if ramadanDateComps.day == getCurrentDateComponents().day{
                    
                    print("same day ifter.........",singleRamadan.value(forKeyPath: "iftar_time")!,singleRamadan.value(forKeyPath: "sehri_finish_time")!)
                    
                    ifterTimelabel.text = singleRamadan.value(forKeyPath: "iftar_time") as? String
                    sehriTimeLabel.text = singleRamadan.value(forKeyPath: "sehri_finish_time") as? String
                    
                    ramadanNoLabel.text = String(describing: ramadanNo!)
                    dateLabel.text = formatter.dateWithDay.string(from: Date())
                }
                
            }
    }
    }
    
    func setAlarmData()
    {
        //isPrayerAlarmOn
        

        
        if (UserDefaults.standard.integer(forKey: "isPrayerAlarmOn") == 1 ){

        getCurrentDateData { (initialIndex) in
            
            for i in initialIndex ..< prayerArray.count{
                
                if i < initialIndex+8
                {
                    let singleDay = prayerArray[i] as! NSDictionary
                    let singleDayPrayerTime = singleDay.value(forKey: "pray_times") as! NSDictionary
                    
                   // print("singleDay : ",singleDay)
                    var dateComp=getCurrentDateComponents()
                    dateComp.day = (singleDay.value(forKey: "day_no") as! NSString).integerValue
                    dateComp.month = (singleDay.value(forKey: "month_no") as! NSString).integerValue
                    
                    print(dateComp)
                    generateAlarmContent(prayerKey: "fajr_time",prayerName: "ফজর", singleDayPrayerTime: singleDayPrayerTime,prayerDateComp: dateComp)
                    generateAlarmContent(prayerKey: "zuhr_time",prayerName: "যোহর", singleDayPrayerTime: singleDayPrayerTime,prayerDateComp: dateComp)
                    generateAlarmContent(prayerKey: "asr_time",prayerName: "আছর", singleDayPrayerTime: singleDayPrayerTime,prayerDateComp: dateComp)
                    
                    if (dateComp.month! == 05 && dateComp.day! > 17) || (dateComp.month! == 06 && dateComp.day! < 17){
                        
                        print("it's ramadan")
                        
                    }else{
                        
                        print("it's not ramadan")
                        generateAlarmContent(prayerKey: "magrib_time",prayerName: "মাগরিব", singleDayPrayerTime: singleDayPrayerTime,prayerDateComp: dateComp)
                    }
                    
//                    generateAlarmContent(prayerKey: "magrib_time",prayerName: "মাগরিব", singleDayPrayerTime: singleDayPrayerTime,prayerDateComp: dateComp)
                    

                    generateAlarmContent(prayerKey: "isha_time",prayerName: "ইশা", singleDayPrayerTime: singleDayPrayerTime,prayerDateComp: dateComp)
                    
                    
                    if i == initialIndex + 7
                    {
                        setReminderForAppLaunch(prayerKey: "isha_time", singleDayPrayerTime: singleDayPrayerTime,prayerDateComp: dateComp)
                        break;
                    }
                    
                }
            }
        }
        }else{
            
            print("Prayer Alarm is off")
        }
        
    }
    
    func setRamadanAlarm(){

        
        let currentDate = Date()
        let nextSevenDays:Date
        
        if currentDate > ramadanstartDate! {
            
            nextSevenDays = Calendar.current.date(byAdding: .day, value: 7 , to: currentDate as Date)!
            
        }else{
            
            nextSevenDays = Calendar.current.date(byAdding: .day, value: 7 , to: ramadanstartDate as Date)!
            
        }
        
        let nextSixDay = Calendar.current.date(byAdding: .day, value: -1 , to: nextSevenDays as Date)!

        
        
        for i in 0..<ramadanArray.count {
            
            
            let singleRamadan = ramadanArray[i] as! NSDictionary
            
            let ramadanNo =  singleRamadan.value(forKey: "ramadan_no")
            
            let ramadanDate = Calendar.current.date(byAdding: .day, value: (ramadanNo as! Int - 1), to: ramadanstartDate! as Date)
            
            var ramadanDateComps = Calendar.current.dateComponents([.year,.month,.day,.hour,.minute,.second], from: ramadanDate!)
            var ramadanDateCompsForIftar = ramadanDateComps
            

            if  ramadanDate! >= currentDate  && ramadanDate! < nextSevenDays
                {
                
              //  print("ramadanDate ....",ramadanDate!)
                if (UserDefaults.standard.integer(forKey: "isSehriAlarmOn") == 1 ){
                    
                    let sehriTimeComps = getTimeComponentsForString(prayerTime: singleRamadan.value(forKey: "sehri_finish_time") as! String)
                    
                    ramadanDateComps.hour = sehriTimeComps.hour
                    ramadanDateComps.minute = sehriTimeComps.minute! - 30
                    ramadanDateComps.second = sehriTimeComps.second
                    
                    
                    
                    generateAlarmContentForRamadan(alarmFor: "সেহরি",ramadanDateComp:ramadanDateComps,alarmTime: singleRamadan.value(forKey: "sehri_finish_time") as! String)
                    
                   //  print("Sehri Alarm is on")
                    
                }else{
                    
                  //  print("Sehri Alarm is off")
                }
                
                if (UserDefaults.standard.integer(forKey: "isIfterAlarmOn") == 1){
                    
                    let iftarTimeComps = getTimeComponentsForString(prayerTime: singleRamadan.value(forKey: "iftar_time") as! String)
                    
                    ramadanDateCompsForIftar.hour = iftarTimeComps.hour
                    ramadanDateCompsForIftar.minute = iftarTimeComps.minute
                    ramadanDateCompsForIftar.second = iftarTimeComps.second
                    
                    generateAlarmContentForRamadan(alarmFor: "ইফতার",ramadanDateComp:ramadanDateCompsForIftar,alarmTime: singleRamadan.value(forKey: "iftar_time") as! String)
                    
               //     print("Ifter Alarm is on")

                }else{
                    
               //     print("Ifter Alarm is off")
                }
                
            }else if ramadanDateComps.month! >= getCurrentDateComponents().month! && ramadanDateComps.day! >= getCurrentDateComponents().day! && ramadanDate! < nextSevenDays {
                
                print("same day........",i)
                
                //  print("ramadanDate ....",ramadanDate!)
                if (UserDefaults.standard.integer(forKey: "isSehriAlarmOn") == 1 ){
                    
                    let sehriTimeComps = getTimeComponentsForString(prayerTime: singleRamadan.value(forKey: "sehri_finish_time") as! String)
                    
                    ramadanDateComps.hour = sehriTimeComps.hour
                    ramadanDateComps.minute = sehriTimeComps.minute! - 30
                    ramadanDateComps.second = sehriTimeComps.second
                    
                    
                    
                    generateAlarmContentForRamadan(alarmFor: "সেহরি",ramadanDateComp:ramadanDateComps,alarmTime: singleRamadan.value(forKey: "sehri_finish_time") as! String)
                    
                    //  print("Sehri Alarm is on")
                    
                }else{
                    
                    //  print("Sehri Alarm is off")
                }
                
                if (UserDefaults.standard.integer(forKey: "isIfterAlarmOn") == 1){
                    
                    let iftarTimeComps = getTimeComponentsForString(prayerTime: singleRamadan.value(forKey: "iftar_time") as! String)
                    
                    ramadanDateCompsForIftar.hour = iftarTimeComps.hour
                    ramadanDateCompsForIftar.minute = iftarTimeComps.minute
                    ramadanDateCompsForIftar.second = iftarTimeComps.second
                    
                    generateAlarmContentForRamadan(alarmFor: "ইফতার",ramadanDateComp:ramadanDateCompsForIftar,alarmTime: singleRamadan.value(forKey: "iftar_time") as! String)
                    
                         print("Ifter Alarm is on")
                    
                }else{
                    
                    //     print("Ifter Alarm is off")
                }
                

                
            }
            
            if ramadanDate! == nextSixDay{
                
            //    print("Rimender set")
                
                let iftarTimeComps = getTimeComponentsForString(prayerTime: singleRamadan.value(forKey: "iftar_time") as! String)
                
             //   print("iftarTimeComps",iftarTimeComps)

                ramadanDateCompsForIftar.hour = iftarTimeComps.hour! + 1
                ramadanDateCompsForIftar.minute = iftarTimeComps.minute
                ramadanDateCompsForIftar.second = iftarTimeComps.second
                
                setReminderForAppLaunchForRamadan(alarmFor: "Reminder",ramadanDateComp:ramadanDateCompsForIftar)
            }
            
            
        }
        
       

        
    }
    
    func generateAlarmContentForRamadan(alarmFor: String,ramadanDateComp:DateComponents,alarmTime: String)
    {
        
        let content = UNMutableNotificationContent()
        content.title = "\(alarmFor) at \(alarmTime)"
        if alarmFor == "ইফতার" {
            
            print("ইফতার")
            content.body = "You can take your Iftar now"
            content.sound = UNNotificationSound(named: "prayer_time_alarm.aac")

            
            
        }else{
            print("সেহরি")

            content.body = "You have 30 minutes left to finish your Sehri"
            content.sound = UNNotificationSound(named: "iftar_sehri_alarm.aac")

        }
        
        //content.body = "You have 5 minutes to left to take your \(alarmFor)"
//        content.sound = UNNotificationSound(named: "iftar_sehri_alarm.aac")

        let trigger = UNCalendarNotificationTrigger(dateMatching: ramadanDateComp,
                                                    repeats: false)

        let identifier = "RamadanAlarmIdentifier:\(String(describing: ramadanDateComp.day))\(String(describing: ramadanDateComp.month))\(String(describing: ramadanDateComp.hour))\(String(describing: ramadanDateComp.minute))"

        let request = UNNotificationRequest(identifier: identifier,
                                            content: content, trigger: trigger)
        center.add(request, withCompletionHandler: { (error) in
            if let error = error {
                print(error)
            }
        })
    }
    
    func setReminderForAppLaunchForRamadan(alarmFor: String,ramadanDateComp:DateComponents)
    {
        
        let content = UNMutableNotificationContent()
        content.title = "Reminder"
        content.body = "Please open your app to get alarm"
        content.sound = UNNotificationSound.default()

        
        let trigger = UNCalendarNotificationTrigger(dateMatching: ramadanDateComp,
                                                    repeats: false)
        
        print("trigger.....",trigger)
        
        let identifier = "RamadanAlarmIdentifier:\(String(describing: ramadanDateComp.day))\(String(describing: ramadanDateComp.month))\(String(describing: ramadanDateComp.hour))\(String(describing: ramadanDateComp.minute))"
        
        let request = UNNotificationRequest(identifier: identifier,
                                            content: content, trigger: trigger)
        center.add(request, withCompletionHandler: { (error) in
            if let error = error {
                print(error)
            }
        })
        
//        center.getPendingNotificationRequests(completionHandler: { requests in
//            print("Pending Request in setRamadanAlarm data: ",requests)
//            for request in requests {
//                print(" Request: ",request)
//            }
//        })
    }
    
    func generateAlarmContent(prayerKey: String,prayerName: String, singleDayPrayerTime: NSDictionary,prayerDateComp:DateComponents)
    {
      
        let content = UNMutableNotificationContent()
        content.title = "\(prayerName) at \(singleDayPrayerTime.value(forKey: prayerKey)as! String)"
        content.body = "Your Prayer time waqt have started"
        content.sound = UNNotificationSound(named: "prayer_time_alarm.aac")
        
        let timeComps = getTimeComponentsForString(prayerTime: singleDayPrayerTime.value(forKey: prayerKey) as! String)
        
        var dateComp = prayerDateComp
        
        dateComp.hour = timeComps.hour
        dateComp.minute = timeComps.minute
        dateComp.second = timeComps.second
        
        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComp,
                                                    repeats: false)
        
        let identifier = "NotIdentifier:\(String(describing: dateComp.day))\(String(describing: dateComp.month))\(String(describing: dateComp.hour))\(String(describing: dateComp.minute))"
        
        let request = UNNotificationRequest(identifier: identifier,
                                            content: content, trigger: trigger)
        center.add(request, withCompletionHandler: { (error) in
            if let error = error {
                print(error)
            }
        })
       
        
//        center.getPendingNotificationRequests(completionHandler: { requests in
//            print("Pending Request: ",requests)
//            for request in requests {
//                print(" Request: .........",request)
//            }
//        })

    }
    
    func setReminderForAppLaunch(prayerKey: String, singleDayPrayerTime: NSDictionary,prayerDateComp:DateComponents) -> Void
    {
        
        let content = UNMutableNotificationContent()
        content.title = "Reminder"
        content.body = "Please Open your app to continue Prayer Alarm"
        content.sound = UNNotificationSound.default()
        
        let timeComps = getTimeComponentsForString(prayerTime: singleDayPrayerTime.value(forKey: prayerKey) as! String)
        
        var dateComp = prayerDateComp
        
        dateComp.hour = timeComps.hour
        dateComp.minute = timeComps.minute! + 5
        dateComp.second = timeComps.second
        
        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComp,
                                                    repeats: false)
        
        let identifier = "AppLaunchReminder"
        
        let request = UNNotificationRequest(identifier: identifier,
                                            content: content, trigger: trigger)
        center.add(request, withCompletionHandler: { (error) in
            if let error = error {
                print(error)
            }
        })
       
        
        
    }
    
    func getCurrentDateComponents() -> DateComponents
    {
        let date = Date()
    
        let currentDateCompunents = Calendar.current.dateComponents([.year,.month,.day,.hour,.minute,.second], from: date as Date)
        
        //print("currentDateCompunents",currentDateCompunents)
        
        return currentDateCompunents
    }
    
    func getTimeComponentsForString(prayerTime timeString:String) -> DateComponents
    {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation:"GMT+6")
        dateFormatter.dateFormat = "hh:mm a"
        
        let date = dateFormatter.date(from:timeString)!
        
        let timeCompunents = Calendar.current.dateComponents([.hour,.minute,.second], from: date as Date)
    
        return timeCompunents
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func parseDivisionData(){
        
        print("selectedDivision in main view parseDivisionData",(UserDefaults.standard.string(forKey: "selectedDivision"))!)
        
        var mainPath:String? = Bundle.main.path(forResource: "dhaka_division_year_prayer_data", ofType: "json")
        
        if (UserDefaults.standard.string(forKey: "selectedDivision")) == "Dhaka" {
            
            print("selected division Dhaka")
            
            mainPath = Bundle.main.path(forResource: "dhaka_division_year_prayer_data", ofType: "json")
            
        }else if ((UserDefaults.standard.string(forKey: "selectedDivision")) == "Chittagong"){
            
            print("selected division Chattagram")
            
            mainPath = Bundle.main.path(forResource: "chattagram_division_year_prayer_data", ofType: "json")
            
        }else if ((UserDefaults.standard.string(forKey: "selectedDivision")) == "Barisal"){
            
            print("selected division barisal")
            
            mainPath = Bundle.main.path(forResource: "barishal_division_year_prayer_data", ofType: "json")
            
        }else if ((UserDefaults.standard.string(forKey: "selectedDivision")) == "Khulna"){
            
            print("selected division khulna")
            
            mainPath = Bundle.main.path(forResource: "khulna_division_year_prayer_data", ofType: "json")
            
        }else if ((UserDefaults.standard.string(forKey: "selectedDivision")) == "Mymensingh"){
            
            print("selected division mymen")
            
            mainPath = Bundle.main.path(forResource: "mymensingh_division_year_prayer_data", ofType: "json")
            
        }else if ((UserDefaults.standard.string(forKey: "selectedDivision")) == "Rajshahi"){
            
            print("selected division raj")
            
            mainPath = Bundle.main.path(forResource: "rajshahi_division_year_prayer_data", ofType: "json")
            
        }else if ((UserDefaults.standard.string(forKey: "selectedDivision")) == "Rangpur"){
            
            print("selected division rang")
            
            mainPath = Bundle.main.path(forResource: "rangpur_division_year_prayer_data", ofType: "json")
            
        }else if ((UserDefaults.standard.string(forKey: "selectedDivision")) == "Sylhet"){
            
            print("selected division sylhet")
            
            mainPath = Bundle.main.path(forResource: "sylhet_division_year_prayer_data", ofType: "json")
            
        }else{
            
            
        }
        
            if let path = mainPath{
                do {
                    let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                    let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves) as? NSDictionary
                    
                    print("division in parse division data",jsonResult?.value(forKeyPath: "division")! as Any)
                    
                    prayerArray = jsonResult?.value(forKeyPath: "year_data_pray_time") as! NSArray
                    
                    //print("jsonResult",jsonResult!)
                    
                    todaysPrayerTime()

                } catch {
                    // handle error
                }
            }

        
    }
    
    func parseRamadanData(){
    
        print("selectedDistrict in parseRamadanData",(UserDefaults.standard.string(forKey: "selectedDistrict"))!)
            
            if let path = Bundle.main.path(forResource: "ramadan_30_day_data_demo", ofType: "json"){
                do {
                    let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                    let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves) as? NSDictionary
                    
                   // ramadanArray = jsonResult?.value(forKeyPath: "ramadan_data") as! NSArray
                    
                    let  tempRamadanArray = jsonResult?.value(forKeyPath: "ramadan_data") as! NSArray

                    ramadanArray = calculateDistricWiseTime(array: tempRamadanArray)
                    
                    //print("ramadanArray in main view",ramadanArray)
                    
//                    populateRamadanView()
                    
                } catch {
                    // handle error
                }
            }
            
            
        
    }
    

    @objc func UpdateTime(){
        
        let userCalendar = Calendar.current
        
        // here we set the current date
        
        let date = Date()
        let components = Calendar.current.dateComponents([.year,.month,.day,.hour,.minute,.second], from: date)
        
        let currentDate = Calendar.current.date(from: components)
        
        
        let unitFlags = Set<Calendar.Component>([.day, .hour, .minute, .second])
        
        //here we change the seconds to hours,minutes and days
        
        let CompetitionDayDifference = userCalendar.dateComponents(unitFlags, from: currentDate!, to: ramadanstartDate)
        
       
        
        //Display the coundown to the label

        counterLabel.text = ("\(String(describing: CompetitionDayDifference.day!)) Days  \(String(describing: CompetitionDayDifference.hour!)) Hours  \(String(describing: CompetitionDayDifference.minute!)) Mins")
        
        if currentDate! >= ramadanstartDate{
            
            ramadanTimeView.isHidden = false
            timerView.isHidden = true
            
        }
        
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
            
           return arrayMenu.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: "menuCell", for: indexPath )
        
        let title : UILabel = cell.contentView.viewWithTag(2) as! UILabel
        let imgIcon : UIImageView = cell.contentView.viewWithTag(1) as! UIImageView
        
        
        title.text = arrayMenu[indexPath.item]["title"]!
        imgIcon.image = UIImage(named: arrayMenu[indexPath.item]["icon"]!)
        
        let currentFontSize = title.font.pointSize
        
      //  print("currentFontSize",currentFontSize)
        
        return cell

    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        
        if indexPath.item == 0 {
            
          //  print("indexPath.item",indexPath.item)
            
            let destViewController : UIViewController = self.storyboard!.instantiateViewController(withIdentifier: "RamadanCalendarViewController") 
            
            UserDefaults.standard.set(prayerArray, forKey: "prayerArray")
            
            self.navigationController!.pushViewController(destViewController, animated: true)

            
        }else if indexPath.item == 1 {
            
            let destViewController : UIViewController = self.storyboard!.instantiateViewController(withIdentifier: "AlQuranViewController")
            
            self.navigationController!.pushViewController(destViewController, animated: true)
            
        }else if indexPath.item == 2 {
            
            let destViewController : UIViewController = self.storyboard!.instantiateViewController(withIdentifier: "PrayerTimesViewController")
            
            self.navigationController!.pushViewController(destViewController, animated: true)
            
        }else if indexPath.item == 3 {
            
//            todaysPrayerTime()
            let destViewController : UIViewController = self.storyboard!.instantiateViewController(withIdentifier: "ZakatCalculatorViewController")
            
            self.navigationController!.pushViewController(destViewController, animated: true)
   
        }else if indexPath.item == 4 {
           
            let destViewController : UIViewController = self.storyboard!.instantiateViewController(withIdentifier: "DuaViewController")
            
            self.navigationController!.pushViewController(destViewController, animated: true)
            
        }else if indexPath.item == 5 {
            
            
            let destViewController : UIViewController = self.storyboard!.instantiateViewController(withIdentifier: "CompassViewController")
            
            self.navigationController!.pushViewController(destViewController, animated: true)

            
        }else if indexPath.item == 6 {
            
            let destViewController : UIViewController = self.storyboard!.instantiateViewController(withIdentifier: "TasbihCounterViewController")
            
            self.navigationController!.pushViewController(destViewController, animated: true)
            
        }else if indexPath.item == 7 {
            
            
            let destViewController : UIViewController = self.storyboard!.instantiateViewController(withIdentifier: "SettingViewController")
            
            self.navigationController!.pushViewController(destViewController, animated: true)
        }

        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let padding: CGFloat =  70
        let collectionViewSize = collectionView.frame.size.width - padding
        
        collectionViewHeight.constant = collectionViewSize/2 * 4 + 100
        
        menuCollectionView.reloadData()

        
        return CGSize(width: collectionViewSize/2, height: collectionViewSize/2)
    }
    

    
    func todaysPrayerTime(){
        
        let calendar = NSCalendar.init(calendarIdentifier: NSCalendar.Identifier.gregorian)
        
        //Now asking the calendar what month are we in today’s date:
        
       let currentMonthInt = (calendar?.component(NSCalendar.Unit.month, from: Date()))!
       let currentMonthString = String(format: "%02d", currentMonthInt)
        //Now asking the calendar what day are we in today’s date:
        
       let currentDayInt = (calendar?.component(NSCalendar.Unit.day, from: Date()))!
       let currentDayString = String(format: "%02d", currentDayInt)
        
       // print("currentMonthString ,currentDayString",currentMonthString,currentDayString)
        let arrCount = prayerArray.count
        for i in 0 ..< arrCount {
            
            let singleDay = prayerArray[i] as! NSDictionary
            
            let dayNo =  singleDay.value(forKey: "day_no") as? String
            let monthNo = singleDay.value(forKey: "month_no") as? String
            
            
            if (dayNo == currentDayString) && (monthNo == currentMonthString){

                print("singleDay prayer time",singleDay)
                
                UserDefaults.standard.set(singleDay, forKey: "singleDayPrayerTime")
                
            }
        }
        
       // print("today prayer time",(UserDefaults.standard.value(forKey: "singleDayPrayerTime"))!)
        
    }
    
    func findDistrictList(){
        
        
        if let path = Bundle.main.path(forResource: "ramadan_time_difference_demo", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves) as? NSDictionary
                
                // print("json",jsonResult!)
                
                print("selected division in main view......",(UserDefaults.standard.string(forKey: "selectedDivision"))!)
                
                if (UserDefaults.standard.string(forKey: "selectedDivision")) == "Dhaka" {
                    
                    districtArray = jsonResult?.value(forKeyPath: "Dhaka") as! NSArray
                    
                    
                }else if ((UserDefaults.standard.string(forKey: "selectedDivision")) == "Chittagong"){
                    
                    districtArray = jsonResult?.value(forKeyPath: "Chittagong") as! NSArray
                    
                }else if ((UserDefaults.standard.string(forKey: "selectedDivision")) == "Barisal"){
                    
                    districtArray = jsonResult?.value(forKeyPath: "Barisal") as! NSArray
                    
                }else if ((UserDefaults.standard.string(forKey: "selectedDivision")) == "Khulna"){
                    
                    districtArray = jsonResult?.value(forKeyPath: "Khulna") as! NSArray
                    
                }else if ((UserDefaults.standard.string(forKey: "selectedDivision")) == "Mymensingh"){
                    
                    districtArray = jsonResult?.value(forKeyPath: "Mymensingh") as! NSArray
                    
                }else if ((UserDefaults.standard.string(forKey: "selectedDivision")) == "Rajshahi"){
                    
                    districtArray = jsonResult?.value(forKeyPath: "Rajshahi") as! NSArray
                    
                }else if ((UserDefaults.standard.string(forKey: "selectedDivision")) == "Rangpur"){
                    
                    districtArray = jsonResult?.value(forKeyPath: "Rangpur") as! NSArray
                    
                }else if ((UserDefaults.standard.string(forKey: "selectedDivision")) == "Sylhet"){
                    
                    districtArray = jsonResult?.value(forKeyPath: "Sylhet") as! NSArray
                    
                }
   
                
            } catch {
                // handle error
            }
            
        }
    }

    
    func calculateDistricWiseTime(array:NSArray) -> NSArray {
        
        // print("districtArray in calculateDistricWiseTime",districtArray)
        
        let selectedDist = (UserDefaults.standard.string(forKey: "selectedDistrict"))
        
        print("selectedDist in calculateDistricWiseTime in main view",selectedDist!)
        
        print("districtArray.count",districtArray.count)
        
        for i in 0 ..< districtArray.count{
            
            let singleDic = districtArray[i] as! NSDictionary
            
            print("(singleDic.value(forKey: district_name)",(singleDic.value(forKey: "district_name") as! String))
            
            if selectedDist == (singleDic.value(forKey: "district_name") as! String){
                
                add_iftar_mins = singleDic.value(forKey: "add_iftar_mins") as! Int
                add_sehri_mins = singleDic.value(forKey: "add_sehri_mins") as! Int
                minus_iftar_mins = singleDic.value(forKey: "minus_iftar_mins") as! Int
                minus_sehri_mins = singleDic.value(forKey: "minus_sehri_mins") as! Int
                
                print("add_iftar_mins.......",add_iftar_mins)
                
            }
        }
        
        let tempArray = array
        let returendArray = NSMutableArray()
        
        
        for i in 0 ..< tempArray.count {
            
            let singleRamadan = tempArray[i] as! NSDictionary
            
            let ifterTime =  singleRamadan.value(forKey: "iftar_time") as? String
            let sehriTime =  singleRamadan.value(forKey: "sehri_finish_time") as? String
            
            
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "h:mm a"
            
            let ifter = dateFormatter.date(from: ifterTime!)
            
            
            let sehri = dateFormatter.date(from: sehriTime!)
            
            
            var timeCompsIfter = Calendar.current.dateComponents([.hour,.minute], from: ifter!)
            
            var timeCompsSehri = Calendar.current.dateComponents([.hour,.minute], from: sehri!)
            
            
            
            let addInIfter = timeCompsIfter.minute! + add_iftar_mins - minus_iftar_mins
            
            
            let addInSehri = timeCompsSehri.minute! + add_sehri_mins - minus_sehri_mins
            
            
            let formatter = DateFormatter.splitTimeFormatter
            
            
            let addedtimeForaifter = formatter.hour.string(from: ifter!) + ":" + String(addInIfter) + " " + formatter.ampm.string(from: ifter!)
            
            
            
            let addedtimeForasehri =  formatter.hour.string(from: sehri!) + ":" + String(addInSehri) + " " + formatter.ampm.string(from: sehri!)
            
            
            
            let newDic = NSMutableDictionary()
            
            let ramadanNo = i + 1
            
            newDic.setValue( ramadanNo , forKeyPath: "ramadan_no")
            newDic.setValue(addedtimeForaifter, forKeyPath: "iftar_time")
            newDic.setValue(addedtimeForasehri, forKeyPath: "sehri_finish_time")
            
            
            
            returendArray.add(newDic)
            
        }
        
       // print("returendArray in main view",returendArray)
        
        return returendArray
    }
    
    @IBAction func settingButtonAction(_ sender: Any) {
        
        let destViewController : UIViewController = self.storyboard!.instantiateViewController(withIdentifier: "SettingViewController")
        
        self.navigationController!.pushViewController(destViewController, animated: true)
        
    }
    
    
    @IBAction func backButtonAction(_ sender: Any) {
        
        
        dismiss(animated: true, completion: nil)
        
    }
    
    override func slideMenuItemSelectedAtIndex(_ index: Int32) {
        
        print("slideMenuItemSelectedAtIndex")
        
       // calculateRamadanStartDate()
        findDistrictList()
        parseRamadanData()
        populateRamadanView()
        
    }
    
    
}
