//
//  PlayerViewController.swift
//  Finix Ramadan Calendar
//
//  Created by Md.Ballal Hossen on 18/5/18.
//  Copyright © 2018 Sujan. All rights reserved.
//

import UIKit
import AVFoundation

class PlayerViewController: UIViewController,AVAudioPlayerDelegate,UITableViewDelegate,UITableViewDataSource {
    
    

    
    @IBOutlet weak var pauseButton: UIButton!
    
    @IBOutlet weak var forwardButton: UIButton!
    
    @IBOutlet weak var backwardButton: UIButton!
    
    @IBOutlet weak var replayButton: UIButton!
    
    @IBOutlet weak var slider: UISlider!
    
    @IBOutlet weak var suraNameLabel: UILabel!
    
    @IBOutlet weak var musicIndicatorView: ESTMusicIndicatorView!
    
    //@IBOutlet weak var progressView: UIProgressView!
    
    @IBOutlet weak var totalDuration: UILabel!
    @IBOutlet weak var currentTime: UILabel!
    
    @IBOutlet weak var playListTableView: UITableView!
    
    var audioPlayer = AVAudioPlayer()
    var currentSoundsIndex : Int?
    var suraList = [String]()
    var isPlayAllOffline : Bool?
    var isPlayAlldownloaded : Bool?
    var isRepeat : Bool?
    var buttonTag : Int?
    var suraName : String?
    
    var nameDic = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Surah Playlist"
        
        currentSoundsIndex = 0
        isRepeat = false
        
        playListTableView.delegate = self
        playListTableView.dataSource = self
        
        readNameJSON()
        
//        if isPlayAllOffline!{
//
//
                playAll()
//
//
//
//        }else if isPlayAlldownloaded! {
//
//
//
//               print("downloaded")
//               playDownloadedSong()
//
//        }else{
//
//            forwardButton.isHidden = true
//            backwardButton.isHidden = true
//            playListTableView.isHidden = true
//
//            playSingleSong()
//        }
        
        print("sura list count",suraList.count)
        
        musicIndicatorView.hidesWhenStopped = false
        musicIndicatorView.tintColor = UIColor("#384D78")
        
    }
    
    func readNameJSON(){
        
        if let path = Bundle.main.path(forResource: "quran_all_surah_name", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves) as? NSDictionary
                
                
                
                nameDic = jsonResult!
                
               // print("nameDic.....",nameDic)
                
                
                
            } catch {
                // handle error
            }
        }
        
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func pauseButtonAction(_ sender: UIButton) {
        
        if (!sender.isSelected) {
            
            print("pause")
            
            musicIndicatorView.state = .paused
            
            audioPlayer.pause()
            
            print("???????",audioPlayer.currentTime)
            
        }
        else {
            
            audioPlayer.play()
            
            musicIndicatorView.state = .playing
        }
        sender.isSelected = !sender.isSelected

    }
    
    
    @IBAction func replayButtonAction(_ sender: UIButton) {
        
        if (!sender.isSelected) {
            
            isRepeat = true
            
        }
        else {
            
            isRepeat = false
            //sender.backgroundColor = UIColor.clear
       }

        sender.isSelected = !sender.isSelected
    }
    
    func playAll(){
        
       
        musicIndicatorView.state = .playing
        
        let surakey = suraList[currentSoundsIndex!]
        
        print("surakey",surakey)
        
        
        if let name = nameDic.value(forKey: surakey) {
            // now name is not nil and the Optional has been unwrapped, so use it
           // print("name from dic",name)
            
            suraNameLabel.text = name as? String
            
        }else{
            
            print("not found")

        }
        let alertSound : NSURL!
        
        let fileWithFormet = suraList[currentSoundsIndex!] + ".mp3"
        
        
        if Bundle.main.path(forResource: fileWithFormet, ofType:nil ) != nil
        {
            alertSound = NSURL(fileURLWithPath: Bundle.main.path(forResource: suraList[currentSoundsIndex!], ofType: "mp3")!)
            
        }else{
            
            let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
            let url = NSURL(fileURLWithPath: path)
            let pathComponent = url.appendingPathComponent(fileWithFormet)

            alertSound = pathComponent! as NSURL
        }
        
        print("alertSound",alertSound)
        
        do {
            
            audioPlayer = try AVAudioPlayer(contentsOf: alertSound as URL)
            
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, with: AVAudioSessionCategoryOptions.mixWithOthers)
            try AVAudioSession.sharedInstance().setActive(true)
            
            audioPlayer.delegate = self
            
            // lblCurrentSongName.text = soundList[currentSoundsIndex]
            
           
            audioPlayer.currentTime = 0
            
            
            
            slider.maximumValue = Float(audioPlayer.duration)
            slider.value = 0.0
            
            slider!.addTarget(self, action: #selector(valueChanged), for: .valueChanged)
        
            //
            
            Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateAudioProgressView), userInfo: nil, repeats: true)
            
           // progressView.setProgress(Float(audioPlayer.currentTime/audioPlayer.duration), animated: false)

            audioPlayer.play()
            print("play")
          
        } catch {
            // couldn't load file :(
        }
        
        
        
        
        
    }
    
    func playDownloadedSong(){
        
        musicIndicatorView.state = .playing
        
        let surakey = suraList[currentSoundsIndex!]
        
        print("surakey",surakey)
        
        
        if let name = nameDic.value(forKey: surakey) {
            // now name is not nil and the Optional has been unwrapped, so use it
            print("name from dic",name)
            
            suraNameLabel.text = name as? String
            
        }else{
            
            print("not found")
            
        }
       
        let fileWithFormet = suraList[currentSoundsIndex!] + ".mp3"
        
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let url = NSURL(fileURLWithPath: path)
        let pathComponent = url.appendingPathComponent(fileWithFormet)

        do {
            
            audioPlayer = try AVAudioPlayer(contentsOf: pathComponent! as URL)
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, with: AVAudioSessionCategoryOptions.mixWithOthers)
            try AVAudioSession.sharedInstance().setActive(true)
            
            audioPlayer.delegate = self
            
            // lblCurrentSongName.text = soundList[currentSoundsIndex]
            
            
            audioPlayer.currentTime = 0
            
            audioPlayer.play()
            print("play")
            
            Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateAudioProgressView), userInfo: nil, repeats: true)
           
        } catch {
            // couldn't load file :(
        }
  
}
    
    func playSingleSong(){
        
        musicIndicatorView.state = .playing
        suraNameLabel.text = suraName
        
        let fileName = (String(format: "%03d", buttonTag! + 1))
        
        let fileWithFormet = fileName + ".mp3"
        
        if Bundle.main.path(forResource: fileWithFormet, ofType:nil ) != nil
        {
            print("main bundle")
            
            let alertSound = NSURL(fileURLWithPath: Bundle.main.path(forResource: fileName, ofType: "mp3")!)
            
            print("alertSound",alertSound)
            
            do {
                
                audioPlayer = try AVAudioPlayer(contentsOf: alertSound as URL)
                try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, with: AVAudioSessionCategoryOptions.mixWithOthers)
                try AVAudioSession.sharedInstance().setActive(true)
                
                audioPlayer.delegate = self
                
                // lblCurrentSongName.text = soundList[currentSoundsIndex]
                
                
                
                
                audioPlayer.currentTime = 0
               
                audioPlayer.play()
                print("play")
                
                Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateAudioProgressView), userInfo: nil, repeats: true)
               
                
            } catch {
                // couldn't load file :(
            }

        }else{
            
            print("in document")

           // let downloadedFileName = fileName + ".mp3"
           
            let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
            let url = NSURL(fileURLWithPath: path)
            if let pathComponent = url.appendingPathComponent(fileWithFormet) {
                let filePath = pathComponent.path
                print("filePath.......",filePath)
                let fileManager = FileManager.default
                
                if fileManager.fileExists(atPath: filePath) {
                    
                    print("FILE AVAILABLE in document")
                    
                    
                    do {
                        
                        audioPlayer = try AVAudioPlayer(contentsOf: pathComponent as URL)
                        try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, with: AVAudioSessionCategoryOptions.mixWithOthers)
                        try AVAudioSession.sharedInstance().setActive(true)
                        
                        audioPlayer.delegate = self
                        
                        // lblCurrentSongName.text = soundList[currentSoundsIndex]
                        
                        
                        
                        audioPlayer.currentTime = 0
                        
                        audioPlayer.play()
                        print("play")
                        
                        Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateAudioProgressView), userInfo: nil, repeats: true)
                        
                        
                        
                    } catch {
                        // couldn't load file :(
                    }

                    
                }else{
                    
                    
                }

        }
            
            
        }
    }
        

    
    @IBAction func forwardButtonAction(_ sender: Any) {
        
        if pauseButton.isSelected {
            
            pauseButton.isSelected = false
        }
        
        
            if currentSoundsIndex! + 1 < suraList.count {
                
                currentSoundsIndex! += 1
                //currentSoundsIndex = currentSoundsIndex! % suraAudioList.count
                
                print("cirrent index",currentSoundsIndex!)
                
                playAll()
            
             }else{
                
                print("no any more")
            }
        

        
    }
    
    @IBAction func backwardButtonAction(_ sender: Any) {
        
        if pauseButton.isSelected {
            
            pauseButton.isSelected = false
        }

        
            if currentSoundsIndex! + 1 <= suraList.count {
                
                if currentSoundsIndex! == 0 {
                    
                    
                }else{
                    
                    currentSoundsIndex! -= 1
                    //currentSoundsIndex = currentSoundsIndex! % suraAudioList.count
                    
                    print("cirrent index",currentSoundsIndex!)
                    
                    playAll()

                }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return suraList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "playListCell")!

        let surakey = suraList[indexPath.row]
        
        
        
        if let name = nameDic.value(forKey: surakey) {
            // now name is not nil and the Optional has been unwrapped, so use it
            //print("name from dic",name)
            
            cell.textLabel?.text = name as? String
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
            
            if audioPlayer.isPlaying{
                
                audioPlayer.stop()
            }
                
            currentSoundsIndex! = indexPath.row
            
            if pauseButton.isSelected {
                
                pauseButton.isSelected = false
            }

             playAll()
            
        

    }
    
    @objc func updateAudioProgressView()
    {
        if audioPlayer.isPlaying
        {
            
            
            slider.value = Float(audioPlayer.currentTime)
            
            
          
            let currentTimeInt = Int(audioPlayer.currentTime)
            let totalTime = Int(audioPlayer.duration)
            

            currentTime.text = (String(format: "%02d.%02d",((currentTimeInt/60) % 60),(currentTimeInt % 60)))
            totalDuration.text = (String(format: "%02d.%02d",((totalTime/60) % 60),(totalTime % 60)))
        }
    }
    
   
    @objc  func valueChanged(sender: UISlider) {
        
        print("..........")
        audioPlayer.currentTime = TimeInterval(slider.value)
        
        let currentTimeInt = Int(audioPlayer.currentTime)

        currentTime.text = (String(format: "%02d.%02d",((currentTimeInt/60) % 60),(currentTimeInt % 60)))
    }

    //
    
    func audioPlayerBeginInterruption(_ player: AVAudioPlayer) {
        
        pauseButton.isSelected = true
    }
    
    func audioPlayerEndInterruption(_ player: AVAudioPlayer, withOptions flags: Int) {
        
        audioPlayer.play()
        pauseButton.isSelected = false
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        
        print(".................")
        
        
            
            if isRepeat!{
                
                playAll()
                
            }else{
                
              if currentSoundsIndex! + 1 < suraList.count {
                
                currentSoundsIndex! += 1
                //currentSoundsIndex = currentSoundsIndex! % suraAudioList.count
                
                print("cirrent index",currentSoundsIndex!)
                
                playAll()
                
              }else{
                
                print("single play")
                pauseButton.isSelected = true
              }
        
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // check if the back button was pressed
        if isMovingFromParentViewController {
            
            audioPlayer.stop()
            
        print("Back button was pressed.")
        }
    }


}
