//
//  PrayerTimesViewController.swift
//  Finix Ramadan Calendar
//
//  Created by Md.Ballal Hossen on 20/4/18.
//  Copyright © 2018 Sujan. All rights reserved.
//

import UIKit

class PrayerTimesViewController: BaseViewController,HADropDownDelegate {

    @IBOutlet weak var dropDown: HADropDown!
    

    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var monthLabel: UILabel!
    
    @IBOutlet weak var sunriseTimeLabel: UILabel!
    
    @IBOutlet weak var fajrTimeLabel: UILabel!
    
    @IBOutlet weak var zuhrTimeLabel: UILabel!
    
    @IBOutlet weak var ashrTimeLabel: UILabel!
    
    @IBOutlet weak var magribTimeLabel: UILabel!
    
    @IBOutlet weak var ishaTimeLabel: UILabel!
    
    var currentMonthInt = 1
    var currentDayInt = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Prayer Times"

        //addSlideMenuButton()
//
//        dropDown.delegate = self
//        dropDown.items = ["Cat", "Mouse", "Horse", "Elephant", "Dog", "Zebra"]
        
        let formatter = DateFormatter.splitDateFormatter
        
        dateLabel.text = formatter.todayDate.string(from: Date())
        dayLabel.text = formatter.day.string(from: Date())
        monthLabel.text = formatter.month.string(from: Date())
       
        
        //print("prayerTime.........",(UserDefaults.standard.value(forKey: "singleDayPrayerTime"))!)
        
        let singleDayPrayerTimeDic = (UserDefaults.standard.value(forKey: "singleDayPrayerTime")) as! NSDictionary
        
        let prayerTimeDic =  singleDayPrayerTimeDic.value(forKey: "pray_times") as!NSDictionary
        
        print("prayerTime.........",singleDayPrayerTimeDic)
        
        sunriseTimeLabel.text = prayerTimeDic.value(forKey: "sunrise_time") as? String
        fajrTimeLabel.text = prayerTimeDic.value(forKey: "fajr_time") as? String
        zuhrTimeLabel.text = prayerTimeDic.value(forKey: "zuhr_time") as? String
        ashrTimeLabel.text = prayerTimeDic.value(forKey: "asr_time") as? String
        magribTimeLabel.text = prayerTimeDic.value(forKey: "magrib_time") as? String
        ishaTimeLabel.text = prayerTimeDic.value(forKey: "isha_time") as? String
        
        print("sunriseTimeLabel.text",sunriseTimeLabel.text!)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        
    }
    
    func didSelectItem(dropDown: HADropDown, at index: Int) {
        
        print("Item selected at index \(index)")
        
        print("dropDown.title",dropDown.title)
        
    }

}
