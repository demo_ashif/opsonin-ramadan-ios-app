//
//  QuranViewController.swift
//  Finix Ramadan Calendar
//
//  Created by Md.Ballal Hossen on 17/5/18.
//  Copyright © 2018 Sujan. All rights reserved.
//

import UIKit


class QuranViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    
    
    
    @IBOutlet weak var pauseButton: UIButton!
    
    var suraList = NSArray()
    let allVerse = NSArray()
    var suraAudioListOffline = [String]()
    var suraAudioListDownloaded = [String]()
    var isDownLoadPossible : Bool?
    
    @IBOutlet weak var suraListTableView: UITableView!
    
    @IBOutlet weak var downloadingView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        self.title = "Bangla Quran"
        suraListTableView.delegate = self
        suraListTableView.dataSource = self
        


        // off when one is downloading
        
        isDownLoadPossible = true
       
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


    
    func makeAudioList(){


            for i in 0 ..< suraList.count{
                
                let fileName = (String(format: "%03d", i + 1))
                
                let fileNameForSearch = fileName + ".mp3"
                
                if Bundle.main.path(forResource: fileNameForSearch, ofType:nil ) != nil
                {
                    
                    suraAudioListOffline.append(fileName)
                    
                }else{
                    
                    let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
                    let url = NSURL(fileURLWithPath: path)
                    if let pathComponent = url.appendingPathComponent(fileNameForSearch) {
                        let filePath = pathComponent.path
                        //print("filePath.......",filePath)
                        let fileManager = FileManager.default
                        
                        if fileManager.fileExists(atPath: filePath) {
                            
                            print("FILE AVAILABLE in document")
                            
                            suraAudioListDownloaded.append(fileName)
                        }
                    }
                }
                
        }
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
       return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return suraList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : SuraListTableViewCell = tableView.dequeueReusableCell(withIdentifier: "suraCell") as! SuraListTableViewCell
        
        
        let tempDic = suraList[indexPath.row] as! NSDictionary
        
        //to remove optional
        if let suraName = tempDic.value(forKey: "bn_name") as? String{
            
            //cell.suraNameBanglaLabel.text = String(format:"\(indexPath.row + 1). \(suraName)")
            cell.suraNameBanglaLabel.text = suraName
        }
        if let suraNo = tempDic.value(forKey: "bn_sl") as? String{
            
            cell.suraNoLabel.text = String(format:"\(suraNo).")
        }
        
        cell.suraTypeLabel.text = tempDic.value(forKey: "type") as? String
        cell.suraNameArabicLabel.text = tempDic.value(forKey: "ar_name") as? String
        
       
        if let ayatNo = tempDic.value(forKey: "verse_count") as? String{
            
            cell.ayathNoLabel.text = "Ayath: \(ayatNo)"
            
        }
        if let engName = tempDic.value(forKey: "tr_name") as? String{
        
            cell.suraNameEnglishLabel.text = engName
        
        }
        
        //cell.playButton.tag = indexPath.section
       // cell.playButton.addTarget(self, action: #selector(playButtonAction), for: .touchUpInside)
        
        //cell.downloadButton.tag = indexPath.section
       // cell.downloadButton.addTarget(self, action: #selector(downloadButtonAction), for: .touchUpInside)
       // cell.downloadButton.isUserInteractionEnabled = isDownLoadPossible!
        
        //(String(format: "%02d", myInt))
        
        let fileName = (String(format: "%03d.mp3", indexPath.row + 1))
        
        print("fileName",fileName)
        
        if Bundle.main.path(forResource: fileName, ofType:nil ) != nil
        {
            print(" exist in main bundle")
            
            
            
//            if let engName = tempDic.value(forKey: "tr_name") as? String{
//
//                cell.suraNameEnglishLabel.text = "Play: \(engName)"
//
//            }
            
//            cell.playButton.isHidden = false
//            cell.downloadButton.isHidden = true
            
        }else {
            
            print("not exist in main bundle")
            
                        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
                        let url = NSURL(fileURLWithPath: path)
                        if let pathComponent = url.appendingPathComponent(fileName) {
                            let filePath = pathComponent.path
                            //print("filePath.......",filePath)
                            let fileManager = FileManager.default
                            
                            if fileManager.fileExists(atPath: filePath) {
                                
                                print("FILE AVAILABLE in document")
                                
//                                cell.playButton.isHidden = false
//                                cell.downloadButton.isHidden = true
                                
//                                if let engName = tempDic.value(forKey: "tr_name") as? String{
//
//                                    cell.suraNameEnglishLabel.text = "Play: \(engName)"
//
//                                }

            
                            } else {
            
                                print("FILE NOT AVAILABLE")
            
//                                if let engName = tempDic.value(forKey: "tr_name") as? String{
//
//                                    cell.suraNameEnglishLabel.text = "Download: \(engName)"
//
//                                }
                                
//                                cell.playButton.isHidden = true
//                                cell.downloadButton.isHidden = false

                            }
                        } else {
                            
                            print("FILE PATH NOT AVAILABLE")
                            
                        }
            
        }
        

      
       
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let tempDic = suraList[indexPath.row] as! NSDictionary
        
        let destViewController = storyboard?.instantiateViewController(withIdentifier: "SingleSuraViewController") as! SingleSuraViewController

        destViewController.suraName = tempDic.value(forKey: "tr_name") as? String
        destViewController.chapterId = tempDic.value(forKey: "chapter_id") as? String
        
        self.navigationController!.pushViewController(destViewController, animated: true)
        
        
        
    }
    
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//
//        let cell = tableView.dequeueReusableCell(withIdentifier: "headerCell") as! UITableViewCell
//
//
//        return cell
//
//    }
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//
//        return 2
//    }
    
    @IBAction func playAllOfflineButtonActoon(_ sender: UIButton) {
        
       
           // playCurrentSong()
        
        let destViewController = storyboard?.instantiateViewController(withIdentifier: "PlayerViewController") as! PlayerViewController
        
        destViewController.suraList = suraAudioListOffline
        destViewController.isPlayAllOffline = true
        destViewController.isPlayAlldownloaded = false
        
        self.navigationController!.pushViewController(destViewController, animated: true)
        
    }
    
    
    @IBAction func playAllDownloadedButtonActoon(_ sender: UIButton) {
        
//        let indexPath = IndexPath(row: 0, section: 10)
//
//        suraListTableView.scrollToRow(at: indexPath as IndexPath, at: .top, animated: true)
        
        print("suraAudioListOffline........",suraAudioListOffline.count)
        print("suraAudioListDownloaded........",suraAudioListDownloaded.count)

        if  suraAudioListDownloaded.count > 0 {
            
            let destViewController = storyboard?.instantiateViewController(withIdentifier: "PlayerViewController") as! PlayerViewController
            
            destViewController.suraList = suraAudioListDownloaded
            destViewController.isPlayAllOffline = false
            destViewController.isPlayAlldownloaded = true
            
            
            self.navigationController!.pushViewController(destViewController, animated: true)
            
        }else{
            
            let alert = UIAlertController(title: "", message: "No Downloaded Surah.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)

            
        }
        
        
    }
    

    @objc func playButtonAction(sender: UIButton){
        
        let buttonTag = sender.tag
        
        let tempdic = suraList[buttonTag] as! NSDictionary
        let suraname = tempdic.value(forKey: "bn_name") as? String
        
        let destViewController = storyboard?.instantiateViewController(withIdentifier: "PlayerViewController") as! PlayerViewController
        
        destViewController.suraList = suraAudioListOffline
        destViewController.isPlayAllOffline = false
        destViewController.isPlayAlldownloaded = false
        destViewController.buttonTag = buttonTag
        destViewController.suraName = suraname
        
      
        
        self.navigationController!.pushViewController(destViewController, animated: true)

        print("buttonTag",buttonTag)
    }
    

   

}
