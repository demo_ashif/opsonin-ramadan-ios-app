//
//  RamadanCalendarViewController.swift
//  Finix Ramadan Calendar
//
//  Created by Md.Ballal Hossen on 20/4/18.
//  Copyright © 2018 Sujan. All rights reserved.
//

import UIKit

class RamadanCalendarViewController: BaseViewController,HADropDownDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var dropDown: HADropDown!
    
    @IBOutlet weak var FirstRamadanCollectionView: UICollectionView!
    
    @IBOutlet weak var secondRamadanCollectionView: UICollectionView!
    
    @IBOutlet weak var thirdRamadanCollectionView: UICollectionView!
    
    @IBOutlet weak var collectionViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var sehriTimeLabelInMainView: UILabel!
    
    @IBOutlet weak var ifterTimeLabelInMainView: UILabel!
    
    @IBOutlet weak var sehriTimeLabelInPrayerView: UILabel!
    
    @IBOutlet weak var ifterTimeLabelInPrayerView: UILabel!
    
    @IBOutlet weak var ramadanNoLabel: UILabel!
    
    @IBOutlet weak var ramadanNoLabelInPrayerView: UILabel!
    @IBOutlet weak var dateLabelInMainView: UILabel!
    
    
    @IBOutlet weak var shadeView: UIView!
    @IBOutlet weak var prayerView: UIView!
    
    @IBOutlet weak var sunriseTimeLabelInPrayerView: UILabel!
    
    @IBOutlet weak var fajrTimeLabelInPrayerView: UILabel!
    
    @IBOutlet weak var zuhrTimeLabelInPrayerView: UILabel!
    
    @IBOutlet weak var asrTimeLabelInPrayerView: UILabel!
    
    @IBOutlet weak var magribTimeLabelInPrayerView: UILabel!
    
    @IBOutlet weak var ishaTimeLabelInPrayerView: UILabel!
    
    @IBOutlet weak var dayLabelInPrayerView: UILabel!
    
    @IBOutlet weak var dateLabelInPrayerView: UILabel!
    
    var firstTen = [[String:Any]]()
    var secondTen = [[String:Any]]()
    var thirdTen = [[String:Any]]()
    
    var firstTenMoonArray = [Dictionary<String,String>]()
    var secondTenMoonArray = [Dictionary<String,String>]()
    var thirdTenMoonArray = [Dictionary<String,String>]()
    
    var selectedItem:Int = 0
    var selectedCollection:Int = 0
    
    var ramadanMonth = 05
    var selectedRamadandate = 0
    
    var prayerArray : NSArray!
    
    var ramadanstartDate : Date!
    
    
    var districtArray =  NSArray()

    var add_iftar_mins : Int = 0
    var add_sehri_mins : Int = 0
    var minus_iftar_mins : Int = 0
    var minus_sehri_mins : Int = 0

    
    

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Ramadan Time"
        //addSlideMenuButton()
        dropDown.delegate = self
       
        print("(UserDefaults.standard.string(forKey: selectedDistrict)",(UserDefaults.standard.string(forKey: "selectedDistrict"))!)
        
        if UserDefaults.standard.object(forKey: "selectedDistrict") != nil{
            
            // exist
            dropDown.title = (UserDefaults.standard.string(forKey: "selectedDistrict"))!
            
            
        }
        else {
            
            dropDown.title = (UserDefaults.standard.string(forKey: "selectedDivision"))!
            
        }
        
        
        
        FirstRamadanCollectionView.delegate = self
        FirstRamadanCollectionView.dataSource = self
        
        secondRamadanCollectionView.delegate = self
        secondRamadanCollectionView.dataSource = self
        
        thirdRamadanCollectionView.delegate = self
        thirdRamadanCollectionView.dataSource = self
        
        findDistrictList()
        
        addMoonIcon()
        
       
        
        prayerView.isHidden = true
        shadeView.isHidden = true
        
        
        prayerArray = (UserDefaults.standard.value(forKey: "prayerArray")) as! NSArray
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
         readJsonAndPopulateView()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func findDistrictList(){
        
        
        if let path = Bundle.main.path(forResource: "ramadan_time_difference_demo", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves) as? NSDictionary
                
               // print("json",jsonResult!)
                
                print("selected division in ramadan view",(UserDefaults.standard.string(forKey: "selectedDivision"))!)
                
                if (UserDefaults.standard.string(forKey: "selectedDivision")) == "Dhaka" {
                    
                    districtArray = jsonResult?.value(forKeyPath: "Dhaka") as! NSArray
                    
                    
                }else if ((UserDefaults.standard.string(forKey: "selectedDivision")) == "Chittagong"){
                    
                    districtArray = jsonResult?.value(forKeyPath: "Chittagong") as! NSArray
                    
                }else if ((UserDefaults.standard.string(forKey: "selectedDivision")) == "Barisal"){
                    
                    districtArray = jsonResult?.value(forKeyPath: "Barisal") as! NSArray
                    
                }else if ((UserDefaults.standard.string(forKey: "selectedDivision")) == "Khulna"){
                    
                    districtArray = jsonResult?.value(forKeyPath: "Khulna") as! NSArray
                    
                }else if ((UserDefaults.standard.string(forKey: "selectedDivision")) == "Mymensingh"){
                    
                    districtArray = jsonResult?.value(forKeyPath: "Mymensingh") as! NSArray
                    
                }else if ((UserDefaults.standard.string(forKey: "selectedDivision")) == "Rajshahi"){
                    
                    districtArray = jsonResult?.value(forKeyPath: "Rajshahi") as! NSArray
                    
                }else if ((UserDefaults.standard.string(forKey: "selectedDivision")) == "Rangpur"){
                    
                    districtArray = jsonResult?.value(forKeyPath: "Rangpur") as! NSArray
                    
                }else if ((UserDefaults.standard.string(forKey: "selectedDivision")) == "Sylhet"){
                    
                    districtArray = jsonResult?.value(forKeyPath: "Sylhet") as! NSArray
                    
                }
                
                var districtName = [String]()
                
                for i in 0 ..< districtArray.count{
                    
                   let singleDistrict = districtArray[i] as! NSDictionary
                   
                   //print("singleDistrict",singleDistrict)
                   
                    districtName.append(singleDistrict.value(forKey: "district_name") as! String)
                    
                }
                
                //print("districtName",districtName)
               
                dropDown.items = districtName
                
            } catch {
                // handle error
            }

        }
    }
    
    func readJsonAndPopulateView() {
        
        
        
        
        if let path = Bundle.main.path(forResource: "ramadan_30_day_data_demo", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves) as? NSDictionary
                
              //  print("json",jsonResult!)
                
                let ramadanArray = jsonResult?.value(forKeyPath: "ramadan_data") as! NSArray
                
                let districRamadanArray = calculateDistricWiseTime(array: ramadanArray)
                
                print("districRamadanArray",districRamadanArray)
                
                //ramadan date
                var dateComponents = DateComponents()
                dateComponents.year = 2018
                dateComponents.month = 5
                dateComponents.day = UserDefaults.standard.integer(forKey: "ramadanStartDate")
                dateComponents.hour = 3
                dateComponents.minute = 46
                
                // Create date from components
                print(dateComponents)
                
                ramadanstartDate = Calendar.current.date(from: dateComponents)! as Date
                let currentDate = Date()
                
                let formatter = DateFormatter.splitDateFormatter
                
                
                if currentDate >= ramadanstartDate! {
                    
                   for i in 0 ..< 30 {
                    
                      let singleRamadan = districRamadanArray[i] as! NSDictionary
                
                      let ramadanNo =  singleRamadan.value(forKey: "ramadan_no")
                
                      let ramadanDate = Calendar.current.date(byAdding: .day, value: (ramadanNo as! Int - 1), to: ramadanstartDate as Date)
                
                      var ramadanDateComps = Calendar.current.dateComponents([.year,.month,.day,.hour,.minute,.second], from: ramadanDate!)
                    
                     if ramadanDateComps.day == getCurrentDateComponents().day{
                        
                        ifterTimeLabelInMainView.text = singleRamadan.value(forKeyPath: "iftar_time") as? String
                        sehriTimeLabelInMainView.text = singleRamadan.value(forKeyPath: "sehri_finish_time") as? String
                        ramadanNoLabel.text = String(describing: ramadanNo!)
                        dateLabelInMainView.text = formatter.dateWithDay.string(from: Date())
                     }
                   
                }
                }else{
                    
                    print("ramadan is not star yet ")
                    ifterTimeLabelInMainView.text = (districRamadanArray[0] as AnyObject).value(forKeyPath: "iftar_time") as? String
                    sehriTimeLabelInMainView.text = (districRamadanArray[0] as AnyObject).value(forKeyPath: "sehri_finish_time") as? String
                    ramadanNoLabel.text = "1"
                    dateLabelInMainView.text = formatter.date.string(from: ramadanstartDate)
                }
                firstTen.removeAll()
                secondTen.removeAll()
                thirdTen.removeAll()
                for i in 0 ..< 10 {
                    
                    
                    firstTen.append(districRamadanArray[i] as! [String : Any])
                    
                }
                for i in 10 ..< 20 {
                    
                    secondTen.append(districRamadanArray[i] as! [String : Any])
                    
                }
                for i in 20 ..< 30 {
                    
                    thirdTen.append(districRamadanArray[i] as! [String : Any])
                    
                }
                
                
//                print("firstTen",firstTen)
//                print("secondTen",secondTen)
//                print("thirdTen",thirdTen)
                

            } catch {
                // handle error
            }
        }
        
      
        
        
    }
    

    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if (collectionView == FirstRamadanCollectionView) {
            
            return 10
            
        }else if (collectionView == secondRamadanCollectionView) {
            
            return 10
            
        }else {
            
            return 10
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        
        if (collectionView == FirstRamadanCollectionView) {
            
            let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: "1stRamadanCell", for: indexPath )
            
            
            
            let title : UILabel = cell.contentView.viewWithTag(201) as! UILabel
            let moonIcon : UIImageView = cell.contentView.viewWithTag(204) as! UIImageView
            
           // title.text = dic.value(forKeyPath: "iftar_time") as? String
            
            title.text = String(indexPath.item + 1)
            moonIcon.image = UIImage(named: firstTenMoonArray[indexPath.item]["icon"]!)
            
            
             return cell
            
        }else if (collectionView == secondRamadanCollectionView) {
            
            let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: "2ndRamadanCell", for: indexPath )
            
          
            
            let title : UILabel = cell.contentView.viewWithTag(202) as! UILabel
            let moonIcon : UIImageView = cell.contentView.viewWithTag(205) as! UIImageView
            
            // title.text = dic.value(forKeyPath: "iftar_time") as? String
            
            title.text = String(indexPath.item + 11)
            moonIcon.image = UIImage(named: secondTenMoonArray[indexPath.item]["icon"]!)
            
             return cell
            
        }else {
            
            let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: "3rdRamadanCell", for: indexPath )
            
//            let dic = thirdTen[indexPath.item] as NSDictionary
//
//            print("dic...",dic)
            
            let title : UILabel = cell.contentView.viewWithTag(203) as! UILabel
            let moonIcon : UIImageView = cell.contentView.viewWithTag(206) as! UIImageView
            
            // title.text = dic.value(forKeyPath: "iftar_time") as? String
            
            title.text = String(indexPath.item + 21)
            moonIcon.image = UIImage(named: thirdTenMoonArray[indexPath.item]["icon"]!)
            
            return cell
        }
  
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let padding: CGFloat =  20
        let collectionViewSize = collectionView.frame.size.width - padding
        
        collectionViewHeight.constant = collectionViewSize/5 * 2 + 25
        
        FirstRamadanCollectionView.reloadData()
        
        return CGSize(width: collectionViewSize/5, height: collectionViewSize/5)
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        
        if (collectionView == FirstRamadanCollectionView) {
            
           selectedItem = indexPath.item
           selectedCollection = 1
            
            calculateDateForramadan()
            
            let dic = firstTen[indexPath.item] as NSDictionary
            
            print("dic",dic)
            
            let  formatter = DateFormatter.splitDateFormatter
            
            let ramadanNo =  dic.value(forKey: "ramadan_no")
            
            let selectedRamadanDate = Calendar.current.date(byAdding: .day, value: (ramadanNo as! Int - 1), to: ramadanstartDate as Date)
            
           // print("selectedRamadanDate",selectedRamadanDate!)
            
            ifterTimeLabelInPrayerView.text = dic.value(forKeyPath: "iftar_time") as? String
            sehriTimeLabelInPrayerView.text = dic.value(forKeyPath: "sehri_finish_time") as? String
            ramadanNoLabelInPrayerView.text = String(describing: dic.value(forKey: "ramadan_no")!)
            dateLabelInPrayerView.text = formatter.date.string(from: selectedRamadanDate!)
            dayLabelInPrayerView.text = formatter.day.string(from: selectedRamadanDate!)
            // showPrayerView()
            
        }else if (collectionView == secondRamadanCollectionView) {
            
            selectedItem = indexPath.item
            selectedCollection = 2
            
            calculateDateForramadan()
            
            let dic = secondTen[indexPath.item] as NSDictionary
            
            print("dic",dic)
            
            let  formatter = DateFormatter.splitDateFormatter
            
            let ramadanNo =  dic.value(forKey: "ramadan_no")
            
            let selectedRamadanDate = Calendar.current.date(byAdding: .day, value: (ramadanNo as! Int - 1), to: ramadanstartDate as Date)
            
            ifterTimeLabelInPrayerView.text = dic.value(forKeyPath: "iftar_time") as? String
            sehriTimeLabelInPrayerView.text = dic.value(forKeyPath: "sehri_finish_time") as? String
            ramadanNoLabelInPrayerView.text = String(describing: dic.value(forKey: "ramadan_no")!)
            dateLabelInPrayerView.text = formatter.date.string(from: selectedRamadanDate!)
            dayLabelInPrayerView.text = formatter.day.string(from: selectedRamadanDate!)
            // showPrayerView()
            
        }else{
            
            selectedItem = indexPath.item
            selectedCollection = 3
            
            calculateDateForramadan()
            
            let dic = thirdTen[indexPath.item] as NSDictionary
            
            print("dic",dic)
            
            let  formatter = DateFormatter.splitDateFormatter
            
            let ramadanNo =  dic.value(forKey: "ramadan_no")
            
            let selectedRamadanDate = Calendar.current.date(byAdding: .day, value: (ramadanNo as! Int - 1), to: ramadanstartDate as Date)
            
            ifterTimeLabelInPrayerView.text = dic.value(forKeyPath: "iftar_time") as? String
            sehriTimeLabelInPrayerView.text = dic.value(forKeyPath: "sehri_finish_time") as? String
            ramadanNoLabelInPrayerView.text = String(describing: dic.value(forKey: "ramadan_no")!)
            dateLabelInPrayerView.text = formatter.date.string(from: selectedRamadanDate!)
            dayLabelInPrayerView.text = formatter.day.string(from: selectedRamadanDate!)
          //  showPrayerView()
        }
        
        
        
    }
    
    func showPrayerView(){
        
        print("selectedItem" , selectedItem)
        
        prayerView.isHidden = false
        shadeView.isHidden = false
    }
    
    func calculateDateForramadan(){
        
        let ramadandate = UserDefaults.standard.integer(forKey: "ramadanStartDate")
        ramadanMonth = 05
        
        if selectedCollection == 1{
            
            
            selectedRamadandate = ramadandate + selectedItem
            
            print("selectedRamadandate",selectedRamadandate,ramadanMonth)
            
        }else if selectedCollection == 2{
            
            
            
            selectedRamadandate = ramadandate + 10 + selectedItem
            
            print("selectedRamadandate",selectedRamadandate)
        
            if selectedRamadandate > 31{
                
                
                
                selectedRamadandate = selectedItem - 4
                ramadanMonth = ramadanMonth + 1
                print("ramadandate.......",selectedRamadandate,ramadanMonth)
            }
        }else{
            
            selectedRamadandate = 6 + selectedItem
            ramadanMonth = ramadanMonth + 1
            print("ramadandate.......?????",selectedRamadandate,ramadanMonth)

        }
        
        print("ramadandate.......?????.........",selectedRamadandate,ramadanMonth)

        calculatePrayerTime()
        
    }
    
    func calculatePrayerTime(){

        
        
        let arrCount = prayerArray.count
        for i in 0 ..< arrCount {

            let singleDay = prayerArray[i] as! NSDictionary

            let dayNo =  singleDay.value(forKey: "day_no") as? String
            let monthNo = singleDay.value(forKey: "month_no") as? String

            let ramadanDayString = String(format: "%02d", selectedRamadandate)
            let ramadanMonthString = String(format: "%02d", ramadanMonth)

            print("dayno",dayNo!)
            print("ramadanDayString",ramadanDayString)

            if (dayNo == ramadanDayString) && (monthNo == ramadanMonthString){

                print("singleDay today in ramadan view",singleDay)

                
                
                let prayerTimeDic =  singleDay.value(forKey: "pray_times") as!NSDictionary
                
                print("prayerTime.........",prayerTimeDic)
                
                sunriseTimeLabelInPrayerView.text = prayerTimeDic.value(forKey: "sunrise_time") as? String
                fajrTimeLabelInPrayerView.text = prayerTimeDic.value(forKey: "fajr_time") as? String
                zuhrTimeLabelInPrayerView.text = prayerTimeDic.value(forKey: "zuhr_time") as? String
                asrTimeLabelInPrayerView.text = prayerTimeDic.value(forKey: "asr_time") as? String
                magribTimeLabelInPrayerView.text = prayerTimeDic.value(forKey: "magrib_time") as? String
                ishaTimeLabelInPrayerView.text = prayerTimeDic.value(forKey: "isha_time") as? String
                
                print("sunriseTimeLabel.text",ishaTimeLabelInPrayerView.text!)

                 showPrayerView()

            }
        }
    }
    
    func getCurrentDateComponents() -> DateComponents
    {
        let date = Date()
        
        let currentDateCompunents = Calendar.current.dateComponents([.year,.month,.day,.hour,.minute,.second], from: date as Date)
        
        //print("currentDateCompunents",currentDateCompunents)
        
        return currentDateCompunents
    }

    
    @IBAction func crossButtonAction(_ sender: Any) {
        
        prayerView.isHidden = true
        shadeView.isHidden = true
    }
    
    func calculateDistricWiseTime(array:NSArray) -> NSArray {
        
       // print("districtArray in calculateDistricWiseTime",districtArray)
        
        let selectedDist = dropDown.title
        
      //  print("selectedDist in calculateDistricWiseTime",selectedDist)
        
        for i in 0 ..< districtArray.count{
            
           let singleDic = districtArray[i] as! NSDictionary
            
            if selectedDist == (singleDic.value(forKey: "district_name") as! String){
                
                add_iftar_mins = singleDic.value(forKey: "add_iftar_mins") as! Int
                add_sehri_mins = singleDic.value(forKey: "add_sehri_mins") as! Int
                minus_iftar_mins = singleDic.value(forKey: "minus_iftar_mins") as! Int
                minus_sehri_mins = singleDic.value(forKey: "minus_sehri_mins") as! Int
                
                print("add_iftar_mins.......",add_iftar_mins)

            }
        }
        
        let tempArray = array
        let returendArray = NSMutableArray()
        
        
        for i in 0 ..< tempArray.count {
            
            let singleRamadan = tempArray[i] as! NSDictionary
            
            let ifterTime =  singleRamadan.value(forKey: "iftar_time") as? String
            let sehriTime =  singleRamadan.value(forKey: "sehri_finish_time") as? String
            
            
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "h:mm a"
            
            let ifter = dateFormatter.date(from: ifterTime!)
            
            
            let sehri = dateFormatter.date(from: sehriTime!)
            
            
            var timeCompsIfter = Calendar.current.dateComponents([.hour,.minute], from: ifter!)
            
            var timeCompsSehri = Calendar.current.dateComponents([.hour,.minute], from: sehri!)
            
            
            
            let addInIfter = timeCompsIfter.minute! + add_iftar_mins - minus_iftar_mins
            
            
            let addInSehri = timeCompsSehri.minute! + add_sehri_mins - minus_sehri_mins
            
            
            let formatter = DateFormatter.splitTimeFormatter
            
            
            let addedtimeForaifter = formatter.hour.string(from: ifter!) + ":" + String(addInIfter) + " " + formatter.ampm.string(from: ifter!)
            
            
            
            let addedtimeForasehri =  formatter.hour.string(from: sehri!) + ":" + String(addInSehri) + " " + formatter.ampm.string(from: sehri!)
            
            
            
            let newDic = NSMutableDictionary()
            
            let ramadanNo = i + 1
            
            newDic.setValue( ramadanNo , forKeyPath: "ramadan_no")
            newDic.setValue(addedtimeForaifter, forKeyPath: "iftar_time")
            newDic.setValue(addedtimeForasehri, forKeyPath: "sehri_finish_time")
            
            
            
            returendArray.add(newDic)
            
        }
        
        
        
        return returendArray
    }
    
    
    
    func didSelectItem(dropDown: HADropDown, at index: Int) {
        
        print("Item selected at index \(index)")
        
       print("dropDown.title",dropDown.title)
        
         readJsonAndPopulateView()
        
    }
    
    func didShow(dropDown: HADropDown) {
        
        print("did show ")
        
        let scrollPoint = CGPoint(x:0.0, y:0.0)
        
        scrollView.setContentOffset(scrollPoint, animated: true)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // check if the back button was pressed
        if isMovingFromParentViewController {
            
            //remove dropdown table if open
            dropDown.table.removeFromSuperview()
            print("Back button was pressed.")
        }
    }
    func addMoonIcon(){
        
        firstTenMoonArray.append(["icon":"moon_1"])
        firstTenMoonArray.append(["icon":"moon_2"])
        firstTenMoonArray.append(["icon":"moon_3"])
        firstTenMoonArray.append(["icon":"moon_4"])
        firstTenMoonArray.append(["icon":"moon_5"])
        firstTenMoonArray.append(["icon":"moon_6"])
        firstTenMoonArray.append(["icon":"moon_7"])
        firstTenMoonArray.append(["icon":"moon_8"])
        firstTenMoonArray.append(["icon":"moon_9"])
        firstTenMoonArray.append(["icon":"moon_10"])
        
        
        secondTenMoonArray.append(["icon":"moon_11"])
        secondTenMoonArray.append(["icon":"moon_12"])
        secondTenMoonArray.append(["icon":"moon_13"])
        secondTenMoonArray.append(["icon":"moon_14"])
        secondTenMoonArray.append(["icon":"moon_15"])
        secondTenMoonArray.append(["icon":"moon_16"])
        secondTenMoonArray.append(["icon":"moon_17"])
        secondTenMoonArray.append(["icon":"moon_18"])
        secondTenMoonArray.append(["icon":"moon_19"])
        secondTenMoonArray.append(["icon":"moon_20"])

        
        thirdTenMoonArray.append(["icon":"moon_21"])
        thirdTenMoonArray.append(["icon":"moon_22"])
        thirdTenMoonArray.append(["icon":"moon_23"])
        thirdTenMoonArray.append(["icon":"moon_24"])
        thirdTenMoonArray.append(["icon":"moon_25"])
        thirdTenMoonArray.append(["icon":"moon_26"])
        thirdTenMoonArray.append(["icon":"moon_27"])
        thirdTenMoonArray.append(["icon":"moon_28"])
        thirdTenMoonArray.append(["icon":"moon_29"])
        thirdTenMoonArray.append(["icon":"moon_30"])

    }
    
   
}


