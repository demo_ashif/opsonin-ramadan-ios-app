//
//  SettingViewController.swift
//  Finix Ramadan Calendar
//
//  Created by Md.Ballal Hossen on 25/4/18.
//  Copyright © 2018 Sujan. All rights reserved.
//

import UIKit
import UserNotifications



class SettingViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    

    let center = UNUserNotificationCenter.current()
    
    @IBOutlet weak var popUpView: UIView!
    
    @IBOutlet weak var sehriSwitch: UISwitch!
    
    @IBOutlet weak var ifterSwitch: UISwitch!
    
    @IBOutlet weak var prayerSwitch: UISwitch!
    
    @IBOutlet weak var prayerLocationLabel: UILabel!
    
    @IBOutlet weak var ramadanLocationLabel: UILabel!
    
    
    @IBOutlet weak var ramadanLocationTableView: UITableView!
    @IBOutlet weak var locationTableView: UITableView!
    
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    
     var prayerLocations = [Dictionary<String,String>]()
     var districtName = [String]()
    
     var districtArray =  NSArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Setting"
        
      //  let tapOnView = UITapGestureRecognizer(target: self, action: #selector(handleTapOnPopOverView))
        
       // popUpView.addGestureRecognizer(tapOnView)
        
        prayerLocations.append(["title":"Dhaka"])
        prayerLocations.append(["title":"Chittagong"])
        prayerLocations.append(["title":"Sylhet"])
        prayerLocations.append(["title":"Rajshahi"])
        prayerLocations.append(["title":"Barisal"])
        prayerLocations.append(["title":"Khulna"])
        prayerLocations.append(["title":"Mymensingh"])
        prayerLocations.append(["title":"Rangpur"])
        
        locationTableView.delegate = self
        locationTableView.dataSource = self
        
        ramadanLocationTableView.delegate = self
        ramadanLocationTableView.dataSource = self
        
        locationTableView.layer.cornerRadius = 5

        let tapOnPrayerLocation = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        
        prayerLocationLabel.addGestureRecognizer(tapOnPrayerLocation)
        
        let tapOnRamadanLocation = UITapGestureRecognizer(target: self, action: #selector(handleTapOnRamadanLocation))
        
        ramadanLocationLabel.addGestureRecognizer(tapOnRamadanLocation)
        
        popUpView.isHidden = true
        
        
        if (UserDefaults.standard.integer(forKey: "isPrayerAlarmOn") == 1 ){
            
            prayerSwitch.setOn(true, animated: false)
            
        }else{
            
             prayerSwitch.setOn(false, animated: false)
        }
        
        if (UserDefaults.standard.integer(forKey: "isSehriAlarmOn") == 1 ){
            
            sehriSwitch.setOn(true, animated: false)
        }else{
            
            sehriSwitch.setOn(false, animated: false)
        }
        
        if (UserDefaults.standard.integer(forKey: "isIfterAlarmOn") == 1 ){
            
            ifterSwitch.setOn(true, animated: false)
            
        }else{
            
            ifterSwitch.setOn(false, animated: false)
        }
        
        
        prayerLocationLabel.text = (UserDefaults.standard.string(forKey: "selectedDivision"))
        ramadanLocationLabel.text = (UserDefaults.standard.string(forKey: "selectedDistrict"))
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @objc func handleTap() {
        
        print("tap on prayerLocationLabel")
        
        popUpView.isHidden = false
        ramadanLocationTableView.isHidden = true
        locationTableView.isHidden = false
    }
    
    @objc func handleTapOnRamadanLocation() {
        
        print("tap on ramadanLocationLabel")
        
        findDistrictList()
        
        popUpView.isHidden = false
        ramadanLocationTableView.isHidden = false
        locationTableView.isHidden = true
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == locationTableView{
            
            return prayerLocations.count
        }else{
            
            return districtName.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == locationTableView{
            
            let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "locationCell")!
            
            let locationLabel : UILabel = cell.contentView.viewWithTag(501) as! UILabel
            
            locationLabel.text = prayerLocations[indexPath.row]["title"]!
            
            return cell
        }else{
            
            let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "ramadanLocationCell")!
            
            let locationLabel : UILabel = cell.contentView.viewWithTag(502) as! UILabel
            
            locationLabel.text = districtName[indexPath.row]
            
            return cell

        }
    }

     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let indexPath = tableView.indexPathForSelectedRow
        
        let currentCell = tableView.cellForRow(at: indexPath!) as! UITableViewCell

        if tableView == locationTableView{
            
            let locationLabel : UILabel = currentCell.contentView.viewWithTag(501) as! UILabel
            
            print("selected text",locationLabel.text!)
            
            prayerLocationLabel.text = locationLabel.text
            ramadanLocationLabel.text = locationLabel.text
            
            UserDefaults.standard.set(indexPath?.row, forKey: "selectedIndex")
            
            UserDefaults.standard.set(prayerLocations[(UserDefaults.standard.integer(forKey: "selectedIndex"))]["title"], forKey: "selectedDivision")
            UserDefaults.standard.set(ramadanLocationLabel.text, forKey: "selectedDistrict")
            
            
            
            popUpView.isHidden = true
            
            center.removeAllPendingNotificationRequests()
            center.removeAllDeliveredNotifications()
            
            let mainVC = MainViewController()
            
            mainVC.calculateRamadanStartDate()
            mainVC.findDistrictList()
            mainVC.parseRamadanData()
            mainVC.parseDivisionData()
            
            mainVC.setAlarmData()
            mainVC.setRamadanAlarm()

        }else{
            
            let locationLabel : UILabel = currentCell.contentView.viewWithTag(502) as! UILabel
            
            print("selected text",locationLabel.text!)
            
            ramadanLocationLabel.text = locationLabel.text
            
            popUpView.isHidden = true
            
            UserDefaults.standard.set(ramadanLocationLabel.text, forKey: "selectedDistrict")
            

            center.removeAllPendingNotificationRequests()
            center.removeAllDeliveredNotifications()
            
            let mainVC = MainViewController()
            
            mainVC.calculateRamadanStartDate()
            mainVC.findDistrictList()
            mainVC.parseRamadanData()
            mainVC.parseDivisionData()

            mainVC.setAlarmData()
            mainVC.setRamadanAlarm()

            
        }
    }
    
    @IBAction func sehriSwitchAction(_ sender: Any) {
        
        if sehriSwitch.isOn {
            
            UserDefaults.standard.set(1, forKey: "isSehriAlarmOn")
            
            center.removeAllPendingNotificationRequests()
            center.removeAllDeliveredNotifications()
            
            let mainVC = MainViewController()
            
            mainVC.calculateRamadanStartDate()
            mainVC.findDistrictList()
            mainVC.parseRamadanData()
            mainVC.parseDivisionData()
            
           
            mainVC.setAlarmData()
            mainVC.setRamadanAlarm()
            
        }
        else {
            
            UserDefaults.standard.set(0, forKey: "isSehriAlarmOn")
            
            center.removeAllPendingNotificationRequests()
            center.removeAllDeliveredNotifications()
            
            let mainVC = MainViewController()
            
            mainVC.calculateRamadanStartDate()
            mainVC.findDistrictList()
            mainVC.parseRamadanData()
            mainVC.parseDivisionData()

            mainVC.setAlarmData()
            mainVC.setRamadanAlarm()
        }
        
    }
    
    @IBAction func ifterSwitchAction(_ sender: Any) {
        
        if ifterSwitch.isOn {
            
            UserDefaults.standard.set(1, forKey: "isIfterAlarmOn")
            
            center.removeAllPendingNotificationRequests()
            center.removeAllDeliveredNotifications()
            
            let mainVC = MainViewController()
            
            mainVC.calculateRamadanStartDate()
            mainVC.findDistrictList()
            mainVC.parseRamadanData()
            mainVC.parseDivisionData()

            mainVC.setAlarmData()
            mainVC.setRamadanAlarm()
            
        }
        else {
            
            UserDefaults.standard.set(0, forKey: "isIfterAlarmOn")
            
            center.removeAllPendingNotificationRequests()
            center.removeAllDeliveredNotifications()
            
            let mainVC = MainViewController()
            
            mainVC.calculateRamadanStartDate()
            mainVC.findDistrictList()
            mainVC.parseRamadanData()
            mainVC.parseDivisionData()

            mainVC.setAlarmData()

            mainVC.setRamadanAlarm()
        }
    }
    
    @IBAction func prayerSwitchAction(_ sender: Any) {
        
        if prayerSwitch.isOn {
            
            UserDefaults.standard.set(1, forKey: "isPrayerAlarmOn")
            
            center.removeAllPendingNotificationRequests()
            center.removeAllDeliveredNotifications()
            
            let mainVC = MainViewController()
            
            mainVC.calculateRamadanStartDate()
            mainVC.findDistrictList()
            mainVC.parseRamadanData()
            mainVC.parseDivisionData()
            
            mainVC.setAlarmData()
            mainVC.setRamadanAlarm()
            
        }
        else {
            
            UserDefaults.standard.set(0, forKey: "isPrayerAlarmOn")
            
            center.removeAllPendingNotificationRequests()
            center.removeAllDeliveredNotifications()
            
            let mainVC = MainViewController()
            
            mainVC.calculateRamadanStartDate()
            mainVC.findDistrictList()
            mainVC.parseRamadanData()
            mainVC.parseDivisionData()

            mainVC.setAlarmData()
            mainVC.setRamadanAlarm()

            
        }
        
    }
    
    func findDistrictList(){
        
        
        if let path = Bundle.main.path(forResource: "ramadan_time_difference_demo", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves) as? NSDictionary
                
                // print("json",jsonResult!)
                
                print("selected division in setting view",(UserDefaults.standard.string(forKey: "selectedDivision"))!)
                
                if (UserDefaults.standard.string(forKey: "selectedDivision")) == "Dhaka" {
                    
                    districtArray = jsonResult?.value(forKeyPath: "Dhaka") as! NSArray
                    
                    
                }else if ((UserDefaults.standard.string(forKey: "selectedDivision")) == "Chittagong"){
                    
                    districtArray = jsonResult?.value(forKeyPath: "Chittagong") as! NSArray
                    
                }else if ((UserDefaults.standard.string(forKey: "selectedDivision")) == "Barisal"){
                    
                    districtArray = jsonResult?.value(forKeyPath: "Barisal") as! NSArray
                    
                }else if ((UserDefaults.standard.string(forKey: "selectedDivision")) == "Khulna"){
                    
                    districtArray = jsonResult?.value(forKeyPath: "Khulna") as! NSArray
                    
                }else if ((UserDefaults.standard.string(forKey: "selectedDivision")) == "Mymensingh"){
                    
                    districtArray = jsonResult?.value(forKeyPath: "Mymensingh") as! NSArray
                    
                }else if ((UserDefaults.standard.string(forKey: "selectedDivision")) == "Rajshahi"){
                    
                    districtArray = jsonResult?.value(forKeyPath: "Rajshahi") as! NSArray
                    
                }else if ((UserDefaults.standard.string(forKey: "selectedDivision")) == "Rangpur"){
                    
                    districtArray = jsonResult?.value(forKeyPath: "Rangpur") as! NSArray
                    
                }else if ((UserDefaults.standard.string(forKey: "selectedDivision")) == "Sylhet"){
                    
                    districtArray = jsonResult?.value(forKeyPath: "Sylhet") as! NSArray
                    
                }
                
                
                
                districtName.removeAll()

                for i in 0 ..< districtArray.count{
                    
                    let singleDistrict = districtArray[i] as! NSDictionary
                    
                    
                    districtName.append(singleDistrict.value(forKey: "district_name") as! String)
                    
                }
                
               
                ramadanLocationTableView.reloadData()
                
            } catch {
                // handle error
            }
            
        }
    }
    
    @objc func handleTapOnPopOverView() {
        
        popUpView.isHidden = true
        
    }


    
    @IBAction func crossButtonAction(_ sender: Any) {
        
        popUpView.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // check if the back button was pressed
        
            print("Back button was pressed.")
        
        UserDefaults.standard.set(1, forKey: "isFromSetting")
        
        
    }
    
}
