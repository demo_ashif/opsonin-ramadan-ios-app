//
//  SingleSuraViewController.swift
//  Finix Ramadan Calendar
//
//  Created by Md.Ballal Hossen on 17/5/18.
//  Copyright © 2018 Sujan. All rights reserved.
//

import UIKit

class SingleSuraViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
   

    var chapterId: String!
    var suraName: String!

     var singleSuraVerseArray = [NSDictionary]()
     let sdLoader = SDLoader()
     var cellHeights: [IndexPath : CGFloat] = [:]

    @IBOutlet weak var verseTableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = suraName
        
        verseTableView.isHidden = true
        
        verseTableView.estimatedRowHeight = 200
        verseTableView.rowHeight = UITableViewAutomaticDimension
        
        self.sdLoader.spinner?.lineWidth = 10
        self.sdLoader.spinner?.spacing = 0.1
        self.sdLoader.spinner?.sectorColor = UIColor("#384D78").cgColor
        self.sdLoader.spinner?.textColor = UIColor("#384D78")
        self.sdLoader.spinner?.animationType = AnimationType.anticlockwise
        self.sdLoader.startAnimating(atView: self.view)

        verseTableView.delegate = self
        verseTableView.dataSource = self
        
        
        DispatchQueue.global(qos: .userInitiated).async {
            
            
            self.readJSONforVerse()
            
            // Bounce back to the main thread to update the UI
            DispatchQueue.main.async {
                
                //print("main thread")
                self.verseTableView.isHidden = false
                self.verseTableView.reloadData()
                self.sdLoader.stopAnimation()
            }
        }
        
        
        
        
       // print("chapterId",chapterId)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func readJSONforVerse(){
        if let path = Bundle.main.path(forResource: "quran_all_verses", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves) as? NSDictionary
                
                
                
               let allVerse = jsonResult?.value(forKeyPath: "verses") as! NSArray
                
                
                for i in 0 ..< allVerse.count{

                    let tempDic = allVerse[i] as! NSDictionary

                    let chapterid = tempDic.value(forKey: "chapter_id") as! String

                    if chapterid == chapterId {

                        //let verseId = tempDic.value(forKey: "verse_id") as! Int

                        singleSuraVerseArray.append(tempDic)
                        
                        
                       
                        
                    }



                }
               
                singleSuraVerseArray.sort {
                    guard let leftValue = $0["verse_id"], let rightValue = $1["verse_id"] else {
                        return false // NOTE: you will need to decide how to handle missing keys.
                    }
                    
                return (leftValue as AnyObject).localizedStandardCompare(rightValue as! String) == .orderedAscending
                }
               // print("singleSuraVerseArray",singleSuraVerseArray)
                
            } catch {
                // handle error
            }
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return singleSuraVerseArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : SingleSuraTableViewCell = tableView.dequeueReusableCell(withIdentifier: "verseCell") as! SingleSuraTableViewCell
        
        print("reload", indexPath.row)
        
        
        let tempDic = singleSuraVerseArray[indexPath.row]

        
        cell.verseNoLabel.text = tempDic.value(forKey: "bnsl") as? String
        cell.verseArabicLabel.text = tempDic.value(forKey: "ar") as? String
        cell.pronunciationLabel.text = tempDic.value(forKey: "transliteration") as? String
        cell.banglaLabel.text = tempDic.value(forKey: "bayaan") as? String


        
        return cell
    }
    
    
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//
//        print("willDisplay cell",cell.frame.size.height)
//
//        cellHeights[indexPath] = cell.frame.size.height
//    }
//
//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//
//
//
////        guard let height = cellHeights[indexPath] else { return 380.0 }
////
//    
//        return 100
//
//    }
    
   


}
