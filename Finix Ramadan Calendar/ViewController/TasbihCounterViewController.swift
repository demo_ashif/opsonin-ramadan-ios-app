//
//  TasbihCounterViewController.swift
//  Finix Ramadan Calendar
//
//  Created by Md.Ballal Hossen on 23/4/18.
//  Copyright © 2018 Sujan. All rights reserved.
//

import UIKit

class TasbihCounterViewController: UIViewController {
    @IBOutlet weak var countNumberLabel: UILabel!
    
    var totalCount = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Tasbih Counter"
        // Do any additional setup after loading the view.
        
        let tapOnView = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        
        countNumberLabel.addGestureRecognizer(tapOnView)
        

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func plusButtonAction(_ sender: Any) {
        
        totalCount += 1
        
        countNumberLabel.text = "\(totalCount)"
    }
    
    @IBAction func minusButtonAction(_ sender: Any) {
        
        totalCount -= 1
        
        if totalCount < 0 {
            
            totalCount = 0
        }
        countNumberLabel.text = "\(totalCount)"
        
    }
    
    
    @IBAction func resetButtonAction(_ sender: Any) {
        
        totalCount = 0
        
        countNumberLabel.text = "\(totalCount)"
    }
    
    
    @objc func handleTap() {
        
        print("tap on view")
        
        var url: URL!
        url = Foundation.URL(string: "www.opsonin-pharma.com")
        
        UIApplication.shared.open(url, options: [:])
    }

}
