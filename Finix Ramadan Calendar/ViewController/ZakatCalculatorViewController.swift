//
//  ZakatCalculatorViewController.swift
//  Finix Ramadan Calendar
//
//  Created by Md.Ballal Hossen on 25/4/18.
//  Copyright © 2018 Sujan. All rights reserved.
//

import UIKit

class ZakatCalculatorViewController: UIViewController,UITextFieldDelegate,HADropDownDelegate{

    
    @IBOutlet weak var minNisabLabel: UILabel!
    
    @IBOutlet weak var dropDown: HADropDown!
    
    @IBOutlet weak var goldPriceTextField: UITextField!
    @IBOutlet weak var goldTextField: UITextField!
    @IBOutlet weak var silverTextField: UITextField!
    @IBOutlet weak var moneyTextField: UITextField!
    @IBOutlet weak var stoneTextField: UITextField!
    @IBOutlet weak var shareTextField: UITextField!
    @IBOutlet weak var savedMoneyTextField: UITextField!
    @IBOutlet weak var fishTextField: UITextField!
    
    @IBOutlet weak var loanTextField: UITextField!
    
    @IBOutlet weak var bottomView: UIView!
    
    @IBOutlet weak var isZakatFarajLabel: UILabel!
    
    @IBOutlet weak var totalValueLabel: UILabel!
    
    @IBOutlet weak var zakatValueLabel: UILabel!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    var minNisab:Double = 386250
    var totalValue:Double = 0.0
    
    var goldPrice:Double = 51500
    var silverPrice:Double = 522
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Zakat Calculator"
        
        bottomView.isHidden = true
        
        goldPriceTextField.delegate = self
        goldTextField.delegate = self
        silverTextField.delegate = self
        moneyTextField.delegate = self
        stoneTextField.delegate = self
        shareTextField.delegate = self
        savedMoneyTextField.delegate = self
        fishTextField.delegate = self
        loanTextField.delegate = self
        
        dropDown.delegate = self
        dropDown.items = ["স্বর্ণ(ভরি)", "রূপা(ভরি)"]
        
        minNisabLabel.text = String(format:"%.02f", minNisab)
            
        print("dropDown.title",dropDown.title)
        let scrollViewTap = UITapGestureRecognizer(target: self, action: #selector(scrollViewTapped))
        scrollViewTap.numberOfTapsRequired = 1
        scrollView.addGestureRecognizer(scrollViewTap)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
        
    }
    
    @objc func keyboardWillShow(notification:NSNotification){
        
        
        var info = notification.userInfo
        
        
        if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            
            let contentInsets: UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardHeight, 0.0)
            scrollView.contentInset = contentInsets
            scrollView.scrollIndicatorInsets = contentInsets
            
            var aRect: CGRect = view.frame
            aRect.size.height -= keyboardHeight
            
            if !aRect.contains(moneyTextField.frame.origin) {
                
                scrollView.scrollRectToVisible(moneyTextField.frame, animated: true)
                
            }
        }
        
        
    }
    
    @objc func keyboardWillHide(notification:NSNotification){
        
        print("keyboard hide")
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInset
        scrollView.scrollIndicatorInsets = contentInset
    }
    
    @objc func scrollViewTapped() {
        print("scrollViewTapped")
        
        goldPriceTextField.resignFirstResponder()
        goldTextField.resignFirstResponder()
        silverTextField.resignFirstResponder()
        moneyTextField.resignFirstResponder()
        stoneTextField.resignFirstResponder()
        shareTextField.resignFirstResponder()
        savedMoneyTextField.resignFirstResponder()
        fishTextField.resignFirstResponder()
        loanTextField.resignFirstResponder()

    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func minNisabButtonAction(_ sender: Any) {
        
      print("dropDown.title",dropDown.title)
        
        if dropDown.title == "স্বর্ণ(ভরি)" {
            
            minNisab = Double(goldPriceTextField.text!)! * 7.5
            
            minNisabLabel.text = String(format:"%.02f", minNisab)
            
        }else{
            
            minNisab = Double(goldPriceTextField.text!)! * 52.5
            
            minNisabLabel.text = String(format:"%.02f", minNisab)
        }
        
    }
   
    
    @IBAction func calculateButtonAction(_ sender: Any) {
        
        if goldTextField.text?.isEmpty ?? false && silverTextField.text?.isEmpty ?? false && moneyTextField.text?.isEmpty ?? false && stoneTextField.text?.isEmpty ?? false && shareTextField.text?.isEmpty ?? false && savedMoneyTextField.text?.isEmpty ?? false && fishTextField.text?.isEmpty ?? false && loanTextField.text?.isEmpty ?? false{
            
            print("nil text")
            
            let alert = UIAlertController(title: "", message: "Please give some input.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        }else{
        
            print("min nisab",minNisab)
            
          if minNisab > 0 {
            
            if goldTextField.text?.isEmpty ?? true {
                
                goldTextField.text = "0"
                
            }
            if silverTextField.text?.isEmpty ?? true {
                
                silverTextField.text = "0"
                
            }
            if moneyTextField.text?.isEmpty ?? true {
                
                moneyTextField.text = "0"
                
            }
            if stoneTextField.text?.isEmpty ?? true {
                
               stoneTextField.text = "0"
                
            }
            if shareTextField.text?.isEmpty ?? true {
                
                shareTextField.text = "0"
                
            }
            if savedMoneyTextField.text?.isEmpty ?? true {
                
                savedMoneyTextField.text = "0"
                
            }
            if fishTextField.text?.isEmpty ?? true {
                
                fishTextField.text = "0"
                
            }
            if loanTextField.text?.isEmpty ?? true {
                
                loanTextField.text = "0"
                
            }
            
              totalValue = Double(goldTextField.text!)! + Double(silverTextField.text!)! + Double(moneyTextField.text!)! + Double(stoneTextField.text!)! + Double(savedMoneyTextField.text!)! + Double(fishTextField.text!)! + Double(shareTextField.text!)! - Double(loanTextField.text!)!
            
              totalValueLabel.text = String(format:"%.02f", totalValue)
            
            bottomView.isHidden = false
            //isZakatFarajLabel.isHidden = true
            
            if totalValue >= minNisab
             {
                isZakatFarajLabel.text = "আপনার উপর যাকাত ফরয হয়েছে"
                
                let zakat = (totalValue * 2.5)/100
                
                zakatValueLabel.text = String(format:"%.02f", zakat)
                
                
            }else{
                
                isZakatFarajLabel.text = "আপনার উপর যাকাত ফরয হয়নি"
            }
          }
        
          
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // check if the back button was pressed
        if isMovingFromParentViewController {
            
            //remove dropdown table if open
            dropDown.table.removeFromSuperview()
            print("Back button was pressed.")
        }
    }
    
    func didSelectItem(dropDown: HADropDown, at index: Int) {
        
        print("Item selected at index \(index)")
        
       // print("dropDown.title",dropDown.title)
        
        if dropDown.title == "স্বর্ণ(ভরি)"{
            
            goldPriceTextField.text = String(goldPrice)
            
         }else{
            
            goldPriceTextField.text = String(silverPrice)
            
        }
        
    }
    
    func didShow(dropDown: HADropDown) {
        
        print("did show ")
        
        let scrollPoint = CGPoint(x:0.0, y:0.0)
        
        scrollView.setContentOffset(scrollPoint, animated: true)
        
    }


}
